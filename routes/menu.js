var express = require("express");
const MenuController = require("../controllers/MenuController");
var router = express.Router();

router.get("/get_specific_menulist/:id", MenuController.specificMenuListForUserDisplay);
router.get("/get_menu_list", MenuController.menuList);
router.post("/add_menu", MenuController.menuStore);
router.put("/update_menu/:id", MenuController.menuUpdate);
router.delete("/delete_menu/:id", MenuController.menuDelete);

module.exports = router;