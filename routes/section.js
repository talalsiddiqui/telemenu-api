var express = require("express");
const SectionController = require("../controllers/SectionController");
var router = express.Router();
router.get("/get_section/:id", SectionController.getSectionById);
router.get("/get_specific_restaurant_section/:id", SectionController.specificRestaurantSectionList);
router.get("/get_specific_item_section_list/:id", SectionController.specificItemandSectionList);
router.get("/get_section_list", SectionController.sectionList);
router.get("/get_sub_section_list", SectionController.subSectionList);
router.get("/get_super_section_list", SectionController.superSectionList);
router.post("/add_section", SectionController.sectionStore);
router.put("/update_section/:id", SectionController.sectionUpdate);
router.delete("/delete_section/:id", SectionController.sectionDelete);

module.exports = router;