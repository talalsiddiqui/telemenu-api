var express = require("express");
const ModifierGroupController = require("../controllers/ModifierGroupController");
var router = express.Router();

router.put("/update_modifier_group_activeness/:id", ModifierGroupController.updateModifierGroupActiveness);
router.get("/get_modifier_group_list", ModifierGroupController.modifierGroupList);
router.post("/add_modifier_group", ModifierGroupController.modifierGroupStore);
router.put("/update_modifier_group/:id", ModifierGroupController.modifierGroupUpdate);
router.delete("/delete_modifier_group/:id", ModifierGroupController.modifierGroupDelete);

module.exports = router;
