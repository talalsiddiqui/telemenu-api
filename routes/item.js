var express = require("express");
const ItemController = require("../controllers/ItemController");
var router = express.Router();

router.get("/get_item_list", ItemController.itemList);
router.get("/get_item/:id", ItemController.specificItemWithModifierGroup);
router.get("/get_specific_restaurant_item/:id", ItemController.specificRestaurantItemList);
router.get("/get_top_selling_item_list/:id", ItemController.topSellingItem);
router.post("/add_item", ItemController.itemStore);
router.put("/update_item/:id", ItemController.itemUpdate);
router.delete("/delete_item/:id", ItemController.itemDelete);

module.exports = router;