var express = require("express");
const RestaurantController = require("../controllers/RestaurantController");
var router = express.Router();

router.get("/get_restaurant_list", RestaurantController.restaurantList);
router.get("/get_restaurant/:id", RestaurantController.restaurantDetail);
router.post("/add_restaurant", RestaurantController.restaurantStore);
router.put("/update_restaurant/:id", RestaurantController.restaurantUpdate);
router.delete("/delete_restaurant/:id", RestaurantController.restaurantDelete);

module.exports = router;
