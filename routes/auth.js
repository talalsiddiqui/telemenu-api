var express = require("express");
const AuthController = require("../controllers/AuthController");

var router = express.Router();

router.post("/register", AuthController.register);
router.post("/login", AuthController.login);
router.get("/restaurant_owners_list", AuthController.restaurantOwersList);
router.get("/get_user_list", AuthController.userList);
router.get("/single_user/:id", AuthController.getSingleUser);
router.put("/update_user/:id", AuthController.updateUser);
router.put("/update_user_activeness/:id", AuthController.updateUserActiveness);

module.exports = router;
