var express = require("express");
const LanguageController = require("../controllers/LanguageController");
var router = express.Router();

router.get("/get_language_list/:id", LanguageController.languageList);
router.post("/add_language", LanguageController.languageStore);

module.exports = router;