var express = require("express");
const CategoryController = require("../controllers/categoryController");
var router = express.Router();

router.get("/get_category_list/:id", CategoryController.specificRestaurantcategoryList);
router.get("/get_category_list", CategoryController.categoryList);
router.get("/get_category/:id", CategoryController.categoryDetail);
router.post("/add_category", CategoryController.categoryStore);
router.put("/update_category/:id", CategoryController.categoryUpdate);
router.delete("/delete_category/:id", CategoryController.categoryDelete);

module.exports = router;