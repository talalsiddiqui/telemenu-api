var express = require("express");
const waiterCallController = require("../controllers/WaiterCallContoller");
var router = express.Router();

router.get("/get_waiter_call_list/:id", waiterCallController.waiterCallList);
router.post("/add_waiter_call", waiterCallController.WaiterCallStore);
router.put("/update_waiter_call/:id", waiterCallController.waiterCallFulfill);

module.exports = router;
