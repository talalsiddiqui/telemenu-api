var express = require("express");
const DashboardController = require("../controllers/DashboardController");
var router = express.Router();
router.get("/dashboard_detail/:id", DashboardController.dashboard);
module.exports = router;