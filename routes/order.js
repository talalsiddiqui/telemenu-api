var express = require("express");
const OrderController = require("../controllers/OrderController");
var router = express.Router();

router.get("/get_order_list/:id", OrderController.orderList);
router.post("/get_order_status_list", OrderController.orderStatusList);
router.post("/add_order", OrderController.orderStore);
router.put("/update_order_status/:id", OrderController.orderStatusUpdate);
router.put("/update_order_item_status/:id", OrderController.orderDetailStatusUpdate);
router.put("/update_order_payment_status/:id", OrderController.orderPaymentStatusUpdate);
router.post("/specific_date_order_list/:id", OrderController.specificDateOrderList);

router.put("/update_order/:id", OrderController.orderUpdate);
router.put("/soft_delete_order/:id", OrderController.softDeleteOrder);
router.delete("/delete_order/:id", OrderController.orderDelete);
module.exports = router;