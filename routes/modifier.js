var express = require("express");
const ModifierController = require("../controllers/ModifierController");
var router = express.Router();

router.get("/get_modifier_list/:id", ModifierController.specificModifierList);
router.get("/get_modifier_list", ModifierController.modifierList);
router.post("/add_modifier", ModifierController.modifierStore);
router.put("/update_modifier/:id", ModifierController.modifierUpdate);
router.delete("/delete_modifier/:id", ModifierController.modifierDelete);

module.exports = router;
