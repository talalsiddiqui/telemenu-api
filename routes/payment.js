var express = require("express");
const PaymentController = require("../controllers/PaymentController");
var router = express.Router();

router.post("/online_payment", PaymentController.paymentStore);
router.post("/bank_alfalah_payment", PaymentController.BankOTPSubmit);
module.exports = router;