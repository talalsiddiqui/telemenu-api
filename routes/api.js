var express = require("express");
var authRouter = require("./auth");
var bookRouter = require("./book");
var restaurantRouter = require("./restaurant");
var categoryRouter = require("./category");
var itemRouter = require("./item");
var modifierRouter = require("./modifier");
var modifierGroupRouter = require("./modiferGroup");
var sectionRouter = require("./section");
var menuRouter = require("./menu");
var orderRouter = require("./order");
var waiterCallRouter = require("./waiterCall");
var languageRouter = require("./language");
var paymentRouter = require("./payment");
var dashboardRouter = require("./dashboard");
var app = express();

app.use("/auth/", authRouter);
app.use("/book/", bookRouter);
app.use("/restaurant/", restaurantRouter);
app.use("/category/", categoryRouter);
app.use("/item/", itemRouter);
app.use("/modifier/", modifierRouter);
app.use("/modifiergroup/", modifierGroupRouter);
app.use("/section/", sectionRouter);
app.use("/menu/", menuRouter);
app.use("/order/", orderRouter)
app.use("/waitercall/", waiterCallRouter)
app.use("/language/", languageRouter)
app.use("/payment/", paymentRouter)
app.use("/dashboard/", dashboardRouter)

module.exports = app;