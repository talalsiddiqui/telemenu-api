const { chai, server, should } = require("./testConfig");
const RestaurantModel = require("../models/RestaurantModel");

/**
 * Test cases to test all the restaurant APIs
 * Covered Routes:
 * (1) Login
 * (2) Store restaurant
 * (3) Get all restaurant
 * (4) Get single restaurant
 * (5) Update restaurant
 * (6) Delete restaurant
 */

describe("Restaurant", () => {
	//Before each test we empty the database
	before((done) => { 
		RestaurantModel.deleteMany({}, (err) => { 
			done();           
		});        
	});

	// Prepare data for testing
	const userTestData = {
		"password":"Test@123",
		"email":"maitraysuthar@test12345.com"
	};

	// Prepare data for testing
	const testData = {
		"name":"testing book",
		"address":"testing book desc",
        "contact_number":"3214htrff4",
        "country":"testing book",
		"restaurant_type":"testing book desc",
        "currency_type":"3214htrff4",
        "payment_method":"3214htrff4"
	};

	/*
  * Test the /POST route
  */
	describe("/POST Login", () => {
        it("it should do user Login for restuarant", (done) => {
			chai.request(server)
				.post("/api/auth/login")
				.send({"email": userTestData.email,"password": userTestData.password})
				.end((err, res) => {
					res.should.have.status(200);
					res.body.should.have.property("message").eql("Login Success.");
					userTestData.token = res.body.data.token;
					done();
				});
        });
        console.log("working token ",userTestData.token)
	});

	/*
  * Test the /POST route
  */
	describe("/POST Restaurant Store", () => {
        console.log("displaing Error ",userTestData.token)
		it("It should send validation error for store restaurant", (done) => {
			chai.request(server)
				.post("/api/restaurant")
				.send()
				.set("Authorization", "Bearer "+ userTestData.token)
				.end((err, res) => {
					res.should.have.status(400);
					done();
				});
		});
	});

	/*
  * Test the /POST route
  */
	describe("/POST Restaurant Store", () => {
		it("It should store restaurant", (done) => {
			chai.request(server)
				.post("/api/restaurant")
				.send(testData)
				.set("Authorization", "Bearer "+ userTestData.token)
				.end((err, res) => {
					res.should.have.status(200);
					res.body.should.have.property("message").eql("Restaurant add Success.");
					done();
				});
		});
	});

	/*
  * Test the /GET route
  */
	describe("/GET All restaurant", () => {
		it("it should GET all the restaurants", (done) => {
			chai.request(server)
				.get("/api/restaurant")
				.set("Authorization", "Bearer "+ userTestData.token)
				.end((err, res) => {
					res.should.have.status(200);
					res.body.should.have.property("message").eql("Operation success");
					testData._id = res.body.data[0]._id;
					done();
				});
		});
	});

	/*
  * Test the /GET/:id route
  */
	describe("/GET/:id restaurant", () => {
		it("it should GET the restaurant", (done) => {
			chai.request(server)
				.get("/api/restaurant/"+testData._id)
				.set("Authorization", "Bearer "+ userTestData.token)
				.end((err, res) => {
					res.should.have.status(200);
					res.body.should.have.property("message").eql("Operation success");
					done();
				});
		});
	});

	/*
  * Test the /PUT/:id route
  */
	describe("/PUT/:id restaurant", () => {
		it("it should PUT the restaurant", (done) => {
			chai.request(server)
				.put("/api/restaurant/"+testData._id)
				.send(testData)
				.set("Authorization", "Bearer "+ userTestData.token)
				.end((err, res) => {
					res.should.have.status(200);
					res.body.should.have.property("message").eql("Book update Success.");
					done();
				});
		});
	});

	/*
  * Test the /DELETE/:id route
  */
	describe("/DELETE/:id restaurant", () => {
		it("it should DELETE the restaurant", (done) => {
			chai.request(server)
				.delete("/api/restaurant/"+testData._id)
				.set("Authorization", "Bearer "+ userTestData.token)
				.end((err, res) => {
					res.should.have.status(200);
					res.body.should.have.property("message").eql("Book delete Success.");
					done();
				});
		});
	});
});