var mongoose = require("mongoose");
const User = require("./../models/UserModel");
const apiResponse = require("./../helpers/apiResponse");
module.exports = function accessValidation(req, res, next) {
    var userid = req.headers['userid'];
    console.log(userid)
    User.findById(userid,(err,user)=>{
        // if(err) return res.status(400).send({success:false,msg:"Error Searching User"});
        if(user.user_type==="teleadmin" ){
            console.log("tele access middleware working fine")
            req.user = user;
            next()
        }else{
            console.log("tele access not validate, Middleware Error")
            return apiResponse.unauthorizedResponse(res, "You are not authorized to do this operation.");
        }      
    })
}