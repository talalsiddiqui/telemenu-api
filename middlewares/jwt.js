const jwt = require("express-jwt");
const secret = process.env.JWT_SECRET;
const apiResponse = require("./../helpers/apiResponse");
module.exports = function authenticate (req, res, next){
	try{
		const handleErrorNext = err => {
			// console.log("error exist",err)
			if (err) {
			  if (
				err.name === 'UnauthorizedError' &&
				err.inner.name === 'TokenExpiredError'
			  ) {
				// res.clearCookie('auth0idToken');
				return apiResponse.unauthorizedResponse(res, "Session Expired");
			  }
			}else{
				next();
			}
		};
		const authen = jwt({
			secret: secret
		})
		authen(req, res, handleErrorNext);
	}catch(error){
		return apiResponse.unauthorizedResponse(res, "Session Expired");
	}
}