const RestaurantModel = require("./../models/RestaurantModel");
const { body,validationResult } = require("express-validator");
const { sanitizeBody } = require("express-validator");
const apiResponse = require("../helpers/apiResponse");
const auth = require("../middlewares/jwt");
const accessValidation = require("../middlewares/userValidation");
var mongoose = require("mongoose");
const ItemModel = require("../models/ItemModel");
const LanguageModel = require("../models/LanguageModel");
const ModifierGroupModel = require("../models/ModifierGroupModel");
mongoose.set("useFindAndModify", false);
const { uid } = require('uid');


exports.specificItemWithModifierGroup = [
	function (req, res) {
		try {
            console.log(req.params.id)
			ItemModel.findById(req.params.id).then((item)=>{
				if(item){
					let modifierGroupIDs=[];
					console.log(item.modifierGroup)
					if(item.modifierGroup){
						item.modifierGroup.map((singleMod)=>{
							modifierGroupIDs.push(singleMod)
						})
					}
					console.log(modifierGroupIDs)
					ModifierGroupModel
					.find({_id:{$in:modifierGroupIDs}})
					.populate('modifier')
					.then((modifierGroups)=>{
						console.log(" modifierGroups working")
						console.log(modifierGroups)
						if(modifierGroups){
							return apiResponse.successResponseWithData(res, "List fetched", {item,modifierGroups});
						}else{
							return apiResponse.successResponseWithData(res, "No data found", []);
						}
					});
				}else{
					return apiResponse.successResponseWithData(res, "No data found", []);
				}
			});
		} catch (err) {
			return apiResponse.ErrorResponse(res, err);
		}
	}
];


exports.specificRestaurantItemList = [
	auth,
	accessValidation,
	function (req, res) {
		try {
			console.log("route working")
			console.log(req.body)
			RestaurantModel
			.findById(req.params.id)
			.exec((err,restaurantfound)=>{
				if(err) return apiResponse.ErrorResponse(res, err)
				if(restaurantfound === null){
					return apiResponse.notFoundResponse(res,"Restaurant not exists with this id");
				}else{
					console.log("restaurant found")
					ItemModel
					.find({restaurant: {$in:restaurantfound._id}})
					.then((items)=>{
						if(items.length > 0){
							return apiResponse.successResponseWithData(res, "List fetched success", items);
						}else{
							return apiResponse.successResponseWithData(res, "No data found", []);
						}
					});
				}
			})
		}catch (err) { 
			return apiResponse.ErrorResponse(res, err);
		}
	}
];

/**
 * Item List.
 * 
 * @returns {Object}
 */

 exports.itemList = [
	auth,
	accessValidation,
	function (req, res) {
		try {
			console.log("route working")
			console.log(req.user)
			if (req.user.user_type ==="owner"){
				RestaurantModel
				.find({owner: req.user._id},'_id owner')
				.exec((err,restaurantList)=>{
					if(err) return apiResponse.ErrorResponse(res, err)
					if(restaurantList === null){
						console.log("restaurant not found")
						return apiResponse.notFoundResponse(res,"Restaurant not exists with this id");
					}else{
						console.log("restaurant found")
						restaurantIDs = restaurantList.map((singleRestaurant)=>{
							return singleRestaurant._id
						})
						ItemModel
						.find({restaurant: {$in:restaurantIDs}})
						.populate([{
							path: 'restaurant',
							select: '_id name'
						}])
						.populate([{
							path: 'category',
							select: '_id name'
						}])
						.then((items)=>{
							if(items.length > 0){
								return apiResponse.successResponseWithData(res, "List fetched success", items);
							}else{
								return apiResponse.successResponseWithData(res, "No data found", []);
							}
						});
					}
				})
			}else{
				ItemModel
				.find({})
				.populate('restaurant','_id name')
				.populate('category','_id name')
				.then((items)=>{
					if(items.length > 0){
						return apiResponse.successResponseWithData(res, "List fetched success", items);
					}else{
						return apiResponse.successResponseWithData(res, "No data found", []);
					}
				});
			}
		} catch (err) { 
			return apiResponse.ErrorResponse(res, err);
		}
	}
];


/**
 * Item store.
 * 
 * @param {string}      title 
 * @param {string}      description
 * @param {string}      isbn
 * 
 * @returns {Object}
 */
exports.itemStore = [
	auth,
	accessValidation,
	body("name", "Name must not be empty.").isLength({ min: 1 }).trim(),
	body("price", "Price must not be empty.").isLength({ min: 1 }).trim(),
	body("category", "Category must not be empty.").isLength({ min: 1 }).trim(),
	body("restaurant", "Restaurant must not be empty.").isLength({ min: 1 }).trim(),
	body("custom_schedule", "Schedule must not be empty.").isLength({ min: 1 }).trim(),
	sanitizeBody("name").escape(),
	sanitizeBody("price").escape(),
	sanitizeBody("category").escape(),
	sanitizeBody("restaurant").escape(),
	sanitizeBody("custom_schedule").escape(),
	(req, res) => {
		try{
			console.log("add rest req",req.body)
			const errors = validationResult(req);
			var item;
			var TransID = req.body.name.trim()
			TransID = `${TransID.replace(/ /g,'_')}_${uid(10)}`
			var TransDesID = req.body.name.trim();
			TransDesID = `${TransDesID.replace(/ /g,'_')}_${uid(10)}_d`
			if(req.body.modifierGroup){
				item = new ItemModel({
					name: req.body.name,
					urdu_name: req.body.urdu_name ,
					description: req.body.description ? req.body.description :"",
					urdu_description: req.body.urdu_description ? req.body.urdu_description :"",
					price: req.body.price,
					restaurant:req.body.restaurant,
					category: req.body.category,
					modifierGroup: req.body.modifierGroup,
					trans_id: TransID,
					trans_des_id: TransDesID,
					no_gluten: req.body.no_gluten ? req.body.no_gluten: false,
					no_nuts: req.body.no_nuts ? req.body.no_nuts: false,
					no_shellfish: req.body.no_shellfish ? req.body.no_shellfish: false,
					vegan: req.body.vegan ? req.body.vegan: false,
					vegetarian: req.body.vegetarian ? req.body.vegetarian: false,
					keto: req.body.keto ? req.body.keto: false,
					custom_schedule: req.body.custom_schedule,
					start_time: req.body.start_time,
					end_time: req.body.end_time,
					item_image: { 
						path: (req.body.item_image && req.body.item_image.path) ? req.body.item_image.path:'',
						name: (req.body.item_image && req.body.item_image.name) ? req.body.item_image.name:''
					},
				});
			}else{
				item = new ItemModel({   
					name: req.body.name,
					urdu_name: req.body.urdu_name,
					description: req.body.description ? req.body.description :"",
					urdu_description: req.body.urdu_description ? req.body.urdu_description :"",
					price: req.body.price,
					restaurant:req.body.restaurant,
					category: req.body.category,
					trans_id: TransID,
					trans_des_id: TransDesID,
					no_gluten: req.body.no_gluten ? req.body.no_gluten: false,
					no_nuts: req.body.no_nuts ? req.body.no_nuts: false,
					no_shellfish: req.body.no_shellfish ? req.body.no_shellfish: false,
					vegan: req.body.vegan ? req.body.vegan: false,
					vegetarian: req.body.vegetarian ? req.body.vegetarian: false,
					keto: req.body.keto ? req.body.keto: false,
					custom_schedule: req.body.custom_schedule,
					start_time: req.body.start_time,
					end_time: req.body.end_time,
					item_image: { 
						path: (req.body.item_image && req.body.item_image.path) ? req.body.item_image.path:'',
						name: (req.body.item_image && req.body.item_image.name) ? req.body.item_image.name:''
					},
				});
			}
			if(!errors.isEmpty()) {
				return apiResponse.validationErrorWithData(res, "Validation Error.", errors.array());
			}
			else{             
				//Save Item.
				item.save(function (err,itemDoc) {
					if (err)  return apiResponse.ErrorResponse(res, err);
					let lang = [
						{
							restaurant: itemDoc.restaurant,
							trans_id: itemDoc.trans_id,
							en_name: itemDoc.name,
							urdu_name: itemDoc.urdu_name ? itemDoc.urdu_name : itemDoc.name,
						},
						{
							restaurant: itemDoc.restaurant,
							trans_id: itemDoc.trans_des_id,
							en_name: itemDoc.description,
							urdu_name: itemDoc.urdu_description ? itemDoc.urdu_description:itemDoc.description,
						},
					]
					LanguageModel.create(lang,(err, langDoc) => {
						if (err) return apiResponse.ErrorResponse(res, err);
						if (req.user.user_type ==="owner"){
							RestaurantModel
							.find({owner: req.user._id},'_id owner')
							.exec((err,restaurantList)=>{
								if(err) return apiResponse.ErrorResponse(res, err)
								if(restaurantList === null){
									console.log("restaurant not found")
									return apiResponse.notFoundResponse(res,"Restaurant not exists with this id");
								}else{
									console.log("restaurant found")
									restaurantIDs = restaurantList.map((singleRestaurant)=>{
										return singleRestaurant._id
									})
									ItemModel
									.find({restaurant: {$in:restaurantIDs}})
									.populate([{
										path: 'restaurant',
										select: '_id name'
									}])
									.populate([{
										path: 'category',
										select: '_id name'
									}])
									.then((items)=>{
										if(items.length > 0){
											return apiResponse.successResponseWithData(res, "Item add success.", items);
										}else{
											return apiResponse.successResponseWithData(res, "Item add success.", []);
										}
									});
								}
							})
						}else{
							ItemModel
							.find({})
							.populate('restaurant','_id name')
							.populate('category','_id name')
							.then((items)=>{
								if(items.length > 0){
									return apiResponse.successResponseWithData(res, "Item add success.", items);
								}else{
									return apiResponse.successResponseWithData(res, "Item add success.", []);
								}
							});
						}
					})
				});
			}
		}catch (err) {
			//throw error in json response with status 500. 
			return apiResponse.ErrorResponse(res, err);
		}
	}
];


/**
 * Item update.
 * 
 * @param {string}      title 
 * @param {string}      description
 * @param {string}      isbn
 * 
 * @returns {Object}
 */
exports.itemUpdate = [
	auth,
	accessValidation,
	body("name", "Name must not be empty.").isLength({ min: 1 }).trim(),
	body("price", "Price must not be empty.").isLength({ min: 1 }).trim(),
	body("category", "Category must not be empty.").isLength({ min: 1 }).trim(),
	body("restaurant", "Restaurant must not be empty.").isLength({ min: 1 }).trim(),
	body("custom_schedule", "Schedule must not be empty.").isLength({ min: 1 }).trim(),
	sanitizeBody("name").escape(),
	sanitizeBody("price").escape(),
	sanitizeBody("category").escape(),
	sanitizeBody("restaurant").escape(),
	sanitizeBody("custom_schedule").escape(),
	(req, res) => {
		try {
			const errors = validationResult(req);
			console.log(req.body)
			var item ;
			if(req.body.modifierGroup){
				item = new ItemModel({   
					name: req.body.name,
					urdu_name: req.body.urdu_name,
					description: req.body.description ? req.body.description :'',
					urdu_description: req.body.urdu_description ? req.body.urdu_description :"",
					price: req.body.price,
					restaurant:req.body.restaurant,
					category: req.body.category,
					modifierGroup: [...req.body.modifierGroup],
					no_gluten: req.body.no_gluten ? req.body.no_gluten: false,
					no_nuts: req.body.no_nuts ? req.body.no_nuts: false,
					no_shellfish: req.body.no_shellfish ? req.body.no_shellfish: false,
					vegan: req.body.vegan ? req.body.vegan: false,
					vegetarian: req.body.vegetarian ? req.body.vegetarian: false,
					keto: req.body.keto ? req.body.keto: false,
					custom_schedule: req.body.custom_schedule,
					start_time: req.body.start_time ? req.body.start_time:new Date(),
					end_time: req.body.end_time ? req.body.end_time:new Date(),
					item_image: { 
						path: (req.body.item_image && req.body.item_image.path) ? req.body.item_image.path:'',
						name: (req.body.item_image && req.body.item_image.name) ? req.body.item_image.name:''
					},
					_id:req.params.id,
				});
			}else{
				item = new ItemModel({   
					name: req.body.name,
					urdu_name: req.body.urdu_name,
					description: req.body.description ? req.body.description :'',
					urdu_description: req.body.urdu_description ? req.body.urdu_description :'',
					price: req.body.price,
					restaurant:req.body.restaurant,
					category: req.body.category,
					no_gluten: req.body.no_gluten ? req.body.no_gluten: false,
					no_nuts: req.body.no_nuts ? req.body.no_nuts: false,
					no_shellfish: req.body.no_shellfish ? req.body.no_shellfish: false,
					vegan: req.body.vegan ? req.body.vegan: false,
					vegetarian: req.body.vegetarian ? req.body.vegetarian: false,
					keto: req.body.keto ? req.body.keto: false,
					custom_schedule: req.body.custom_schedule,
					start_time: req.body.start_time ? req.body.start_time:new Date(),
					end_time: req.body.end_time ? req.body.end_time:new Date(),
					item_image: { 
						path: (req.body.item_image && req.body.item_image.path) ? req.body.item_image.path:'',
						name: (req.body.item_image && req.body.item_image.name) ? req.body.item_image.name:''
					},
					_id:req.params.id
				});
			}
			if (!errors.isEmpty()) {
				return apiResponse.validationErrorWithData(res, "Validation Error.", errors.array());
			}
			else {
				if(!mongoose.Types.ObjectId.isValid(req.params.id)){
					return apiResponse.validationErrorWithData(res, "Invalid Error.", "Invalid ID");
				}else{
					ItemModel.findById(req.params.id, function (err, foundItem) {
						if(foundItem === null){
							return apiResponse.notFoundResponse(res,"Item not exists with this id");
						}else{
							//update restaurant.
							console.log("item to update",item)
							ItemModel.findByIdAndUpdate(req.params.id, item, {},function (err,itemDOC) {
								if (err) { 
									return apiResponse.ErrorResponse(res, err); 
								}else{
									console.log("after item update",itemDOC)
									LanguageModel.findOneAndUpdate(
										{
											restaurant:itemDOC.restaurant,
											trans_id:itemDOC.trans_id},
											{ 
												en_name:req.body.name,
												urdu_name: req.body.urdu_name ? req.body.urdu_name: req.body.name },
											{},(err,afterupdate)=>{
										if (err) {
											return apiResponse.ErrorResponse(res, err); 
										}else{
											LanguageModel.findOneAndUpdate(
												{
													restaurant:itemDOC.restaurant,
													trans_id:itemDOC.trans_des_id
												},{ 
													en_name:req.body.description,
													urdu_name:req.body.urdu_description ? req.body.urdu_description : req.body.description
												},{},(err,afterDesUpdate)=>{
												if (err) { 
													return apiResponse.ErrorResponse(res, err); 
												}else{
													console.log("after description update",afterDesUpdate)
													if (req.user.user_type ==="owner"){
														RestaurantModel
														.find({owner: req.user._id},'_id owner')
														.exec((err,restaurantList)=>{
															if(err) return apiResponse.ErrorResponse(res, err)
															if(restaurantList === null){
																console.log("restaurant not found")
																return apiResponse.notFoundResponse(res,"Restaurant not exists with this id");
															}else{
																console.log("restaurant found")
																restaurantIDs = restaurantList.map((singleRestaurant)=>{
																	return singleRestaurant._id
																})
																ItemModel
																.find({restaurant: {$in:restaurantIDs}})
																.populate([{
																	path: 'restaurant',
																	select: '_id name'
																}])
																.populate([{
																	path: 'category',
																	select: '_id name'
																}])
																.then((items)=>{
																	if(items.length > 0){
																		return apiResponse.successResponseWithData(res, "Item update Success.", items);
																	}else{
																		return apiResponse.successResponseWithData(res, "Item update Success.", []);
																	}
																});
															}
														})
													}else{
														ItemModel
														.find({})
														.populate('restaurant','_id name')
														.populate('category','_id name')
														.then((items)=>{
															if(items.length > 0){
																return apiResponse.successResponseWithData(res, "Item update Success.", items);
															}else{
																return apiResponse.successResponseWithData(res, "Item update Success.", []);
															}
														});
													}
												}	
											})	
										}	
									})
								}
							});
						}
					});
				}
			}
		} catch (err) {
			//throw error in json response with status 500. 
			return apiResponse.ErrorResponse(res, err);
		}
	}
];

/**
 * Item Delete.
 * 
 * @param {string}      id
 * 
 * @returns {Object}
 */
exports.itemDelete = [
	auth,
	accessValidation,
	function (req, res) {
		console.log(req.params)
		if(!mongoose.Types.ObjectId.isValid(req.params.id)){
			return apiResponse.validationErrorWithData(res, "Invalid Error.", "Invalid ID");
		}
		try {
			ItemModel.findById(req.params.id, function (err, foundItem) {
				if(foundItem === null){
					return apiResponse.notFoundResponse(res,"Item not exists with this id");
				}else{
					//delete Restaurant.
					ItemModel.findByIdAndRemove(req.params.id,function (err) {
						if (err) { 
							return apiResponse.ErrorResponse(res, err); 
						}else{
							if (req.user.user_type ==="owner"){
								RestaurantModel
								.find({owner: req.user._id},'_id owner')
								.exec((err,restaurantList)=>{
									if(err) return apiResponse.ErrorResponse(res, err)
									if(restaurantList === null){
										console.log("restaurant not found")
										return apiResponse.notFoundResponse(res,"Restaurant not exists with this id");
									}else{
										console.log("restaurant found")
										restaurantIDs = restaurantList.map((singleRestaurant)=>{
											return singleRestaurant._id
										})
										ItemModel
										.find({restaurant: {$in:restaurantIDs}})
										.populate([{
											path: 'restaurant',
											select: '_id name'
										}])
										.populate([{
											path: 'category',
											select: '_id name'
										}])
										.then((items)=>{
											if(items.length > 0){
												return apiResponse.successResponseWithData(res, "Item delete success.", items);
											}else{
												return apiResponse.successResponseWithData(res, "Item delete success.", []);
											}
										});
									}
								})
							}else{
								ItemModel
								.find({})
								.populate('restaurant','name')
								.populate('category','name')
								.then((items)=>{
									if(items.length > 0){
										return apiResponse.successResponseWithData(res, "Item delete success.", items);
									}else{
										return apiResponse.successResponseWithData(res, "Item delete success.", []);
									}
								});
							}
						}
					});
				}
			});
		} catch (err) {
			//throw error in json response with status 500. 
			return apiResponse.ErrorResponse(res, err);
		}
	}
];

exports.topSellingItem = [
	// auth,
	// accessValidation,
	function (req, res) {
	  if (!mongoose.Types.ObjectId.isValid(req.params.id)) {
		return apiResponse.validationErrorWithData(
		  res,
		  "Invalid Restaurant Id",
		  ""
		);
	  }
	  try {
		ItemModel.find({ restaurant: req.params.id })
		  .sort({sell_count: -1})
		  .limit(10)
		  .populate('restaurant')
		  .then((top_Selling_items) => {
			if (top_Selling_items.length > 0) {
			  return apiResponse.successResponseWithData(
				res,
				"List fetched success",
				top_Selling_items
			  );
			} else {
			  return apiResponse.successResponseWithData(
				res,
				"No data found",
				[]
			  );
			}
		  });
	  } catch (err) {
		return apiResponse.ErrorResponse(res, err);
	  }
	},
];