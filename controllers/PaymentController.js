const OrderModel = require("../models/OrderModel");
const BankAPIResponseModel = require('./../models/BankAPIResponseModel')
const Restaurant = require("../models/RestaurantModel");
const ItemModel = require("../models/ItemModel");
const ModifierModel = require("../models/ModifierModel");
const axios = require("axios");
const { body, validationResult } = require("express-validator");
const { sanitizeBody } = require("express-validator");
const apiResponse = require("../helpers/apiResponse");
const auth = require("../middlewares/jwt");
const accessValidation = require("../middlewares/userValidation");
var mongoose = require("mongoose");
const OrderDetailModel = require("../models/OrderDetailModel");
mongoose.set("useFindAndModify", false);
const { uid } = require("uid");
const https = require("https");

/**
 * Category store.
 *
 * @param {string}      title
 * @param {string}      description
 * @param {string}      isbn
 *
 * @returns {Object}
 */
// const frontendData={
// 	customer:"12345678",
// 	items:[
// 		{id:"12345678",qty:"3"},
// 		{id:"12345678",qty:"2"}
// 	],
// 	modifiers:[
// 		{id:"12345678",qty:"3"},
// 	],
// }
exports.paymentStore = [
  // auth,
  // accessValidation,
  body("paymentMode", "Restaurant must not be empty.")
    .isLength({ min: 1 })
    .trim(),
  body("clientAccountNumber", "Account Number must not be empty.")
    .isLength({ min: 1 })
    .trim(),
  sanitizeBody("paymentMode").escape(),
  sanitizeBody("clientAccountNumber").escape(),
  async (req, res) => {
    try {
      console.log(req.body);
      const errors = validationResult(req);
      if (!errors.isEmpty()) {
        return apiResponse.validationErrorWithData(
          res,
          "Validation Error.",
          errors.array()
        );
      }
      const orderDetails = await OrderModel.find( {_id:{$in:req.body.order}} );
      console.log("fetched order", orderDetails);
      let totalBill = 0;
      orderDetails.map((SingleOrder)=>{
        totalBill += SingleOrder.total_price
      })
      console.log("total price",totalBill)
      let PaymentDetails = {
        app_id: "2452014243",
        app_key: "24490245",
        name: "ZOHAIB",
        feeTypeCode: "GENERALPAYMENT",
        mobile: "+923123456789",
        currency: "PKR",
        amount: totalBill,
        // "email":"zohaibshahid915@gmail.com",
        orderId: req.body.order[0],
        orderDesc:"test API",
        paymentMode: req.body.paymentMode,
        clientAccountNumber: req.body.clientAccountNumber,
      };
      if (req.body.paymentMode === "JAZZCASHWALLET") {
        PaymentDetails["cnic"] = req.body.cnic;
      }
    //   if (PaymentDetails.is_cancel) {
    //     apiResponse.cascadeErrorWithData(
    //       res,
    //       "Invalid Attempt. Order already cancel",
    //       " "
    //     );
    //   } else {
        const agent = new https.Agent({
          rejectUnauthorized: false,
        });
        if(req.body.paymentMode === "ALFAACCOUNT"){
          PaymentDetails.mobile = "923215123648";
          const AlfalahConnectDot = await axios.post(
            "https://srv.connectdotnet.com/WebSrv/api/MMPayment/Initiate",
            PaymentDetails,
            { httpsAgent: agent }
          ).then((alfalahAxios)=>{
            console.log("alfalah should wait ",alfalahAxios.data)
            const alfalahAxiosRes= alfalahAxios.data;
            if(alfalahAxiosRes.status_code === 0) {
              console.log("alfalah error working")
              return apiResponse.validationErrorWithData(
                res,
                `Validation Error, ${alfalahAxiosRes.status_message}`,
                ""
              );
            }else{
              if (alfalahAxiosRes.status_code === 1) {
                console.log(" alfalah working")
                var bankAPIresObj = new BankAPIResponseModel({   
                  order: req.body.order[0],
                  authToken: alfalahAxiosRes.authToken,
                  hashKey: alfalahAxiosRes.hashKey,
                  transactionReferenceNumber: alfalahAxiosRes.transactionReferenceNumber,
                  transactionTypeId: alfalahAxiosRes.transactionTypeId,
                  isOtp: alfalahAxiosRes.isOtp,
                  customerMobile: req.body.clientAccountNumber,
                });
                bankAPIresObj.save(function (err,bankAPIResDetail) {
                  if (err)  return apiResponse.ErrorResponse(res, err);
                  console.log("alfalah end ",bankAPIResDetail)
                  return apiResponse.successResponseWithData(
                    res,
                    "payment initialize",
                    {
                      type:"alfalah",
                      ...bankAPIResDetail
                    }
                  );
                })
              }
            }
          }).catch((innerError)=>{
            console.log("inner error working",innerError)
            return apiResponse.validationErrorWithData(
              res,
              `Internal Server Error`,
              ""
            );
          });
        }else{
          const ConnectDot = await axios.post(
            "https://srv.connectdotnet.com/WebSrv/api/MMPayment/Initiate",
            PaymentDetails,
            { httpsAgent: agent }
          ).then((axiosRes)=>{
            console.log("should wait ",axiosRes.data)
            const ConnectDotRes= axiosRes.data;
            if (ConnectDotRes.status_code === 0) {
                console.log("error working")
                return apiResponse.validationErrorWithData(
                    res,
                    `Validation Error, ${ConnectDotRes.status_message}`,
                    ""
                );
            } else {
              if (ConnectDotRes.status_code === 1) {
                console.log(" working")
                OrderModel.updateMany(
                  {_id:{$in:req.body.order}} ,
                  { payment_status: true, $set: {mobile: req.body.clientAccountNumber} },
                  {},
                  function (err) {
                    if (err) {
                      return apiResponse.ErrorResponse(res, err);
                    }else {
                      console.log("working order")
                      return apiResponse.successResponseWithData(
                        res,
                        "payment success",
                        []
                      );
                    }
                  }
                );
              }
            }
          }).catch((innerError)=>{
            console.log("inner error working",innerError)
            return apiResponse.validationErrorWithData(
              res,
              `Internal Server Error`,
              ""
            );
          });
        }
        console.log("end of API")
    } catch (err) {
      console.log(err);
      //throw error in json response with status 500.
      return apiResponse.ErrorResponse(res, err);
    }
  },
];

// exports.paymentStore = [
//   // auth,
//   // accessValidation,
//   body("paymentMode", "Restaurant must not be empty.")
//     .isLength({ min: 1 })
//     .trim(),
//   body("clientAccountNumber", "Account Number must not be empty.")
//     .isLength({ min: 1 })
//     .trim(),
//   sanitizeBody("paymentMode").escape(),
//   sanitizeBody("clientAccountNumber").escape(),
//   async (req, res) => {
//     try {
//       console.log(req.body);
//       const errors = validationResult(req);
//       if (!errors.isEmpty()) {
//         return apiResponse.validationErrorWithData(
//           res,
//           "Validation Error.",
//           errors.array()
//         );
//       }
//       const orderDetails = await OrderModel.findById(req.body.order);
//       console.log("fetched order", orderDetails);
//       let PaymentDetails = {
//         app_id: "2452014243",
//         app_key: "24490245",
//         name: "ZOHAIB",
//         feeTypeCode: "GENERALPAYMENT",
//         mobile: "+923123456789",
//         currency: "PKR",
//         amount: orderDetails.total_price,
//         // "email":"zohaibshahid915@gmail.com",
//         orderId: req.body.order,
//         orderDesc:"test API",
//         paymentMode: req.body.paymentMode,
//         clientAccountNumber: req.body.clientAccountNumber,
//       };
//       if (req.body.paymentMode === "JAZZCASHWALLET") {
//         PaymentDetails["cnic"] = req.body.cnic;
//       }
//     //   if (PaymentDetails.is_cancel) {
//     //     apiResponse.cascadeErrorWithData(
//     //       res,
//     //       "Invalid Attempt. Order already cancel",
//     //       " "
//     //     );
//     //   } else {
//         const agent = new https.Agent({
//           rejectUnauthorized: false,
//         });
//         if(req.body.paymentMode === "ALFAACCOUNT"){
//           PaymentDetails.mobile = "923215123648";
//           const AlfalahConnectDot = await axios.post(
//             "https://srv.connectdotnet.com/WebSrv/api/MMPayment/Initiate",
//             PaymentDetails,
//             { httpsAgent: agent }
//           ).then((alfalahAxios)=>{
//             console.log("alfalah should wait ",alfalahAxios.data)
//             const alfalahAxiosRes= alfalahAxios.data;
//             if(alfalahAxiosRes.status_code === 0) {
//               console.log("alfalah error working")
//               return apiResponse.validationErrorWithData(
//                 res,
//                 `Validation Error, ${alfalahAxiosRes.status_message}`,
//                 ""
//               );
//             }else{
//               if (alfalahAxiosRes.status_code === 1) {
//                 console.log(" alfalah working")
//                 var bankAPIresObj = new BankAPIResponseModel({   
//                   order: req.body.order,
//                   authToken: alfalahAxiosRes.authToken,
//                   hashKey: alfalahAxiosRes.hashKey,
//                   transactionReferenceNumber: alfalahAxiosRes.transactionReferenceNumber,
//                   transactionTypeId: alfalahAxiosRes.transactionTypeId,
//                   isOtp: alfalahAxiosRes.isOtp,
//                   customerMobile: req.body.clientAccountNumber,
//                 });
//                 bankAPIresObj.save(function (err,bankAPIResDetail) {
//                   if (err)  return apiResponse.ErrorResponse(res, err);
//                   console.log("alfalah end ",bankAPIResDetail)
                  
//                   return apiResponse.successResponseWithData(
//                     res,
//                     "payment initialize",
//                     {
//                       type:"alfalah",
//                       ...bankAPIResDetail
//                     }
//                   );
//                 })
//               }
//             }
//           }).catch((innerError)=>{
//             console.log("inner error working",innerError)
//             return apiResponse.validationErrorWithData(
//               res,
//               `Internal Server Error`,
//               ""
//             );
//           });
//         }else{
//           const ConnectDot = await axios.post(
//             "https://srv.connectdotnet.com/WebSrv/api/MMPayment/Initiate",
//             PaymentDetails,
//             { httpsAgent: agent }
//           ).then((axiosRes)=>{
//             console.log("should wait ",axiosRes.data)
//             const ConnectDotRes= axiosRes.data;
//             if (ConnectDotRes.status_code === 0) {
//                 console.log("error working")
//                 return apiResponse.validationErrorWithData(
//                     res,
//                     `Validation Error, ${ConnectDotRes.status_message}`,
//                     ""
//                 );
//             } else {
//               if (ConnectDotRes.status_code === 1) {
//                 console.log(" working")
//                 OrderModel.findByIdAndUpdate(
//                   req.body.order,
//                   { payment_status: true, $set: {mobile: req.body.clientAccountNumber} },
//                   {},
//                   function (err) {
//                     if (err) {
//                       return apiResponse.ErrorResponse(res, err);
//                     }else {
//                       console.log("working order")
//                       return apiResponse.successResponseWithData(
//                         res,
//                         "payment success",
//                         []
//                       );
//                     }
//                   }
//                 );
//               }
//             }
//           }).catch((innerError)=>{
//             console.log("inner error working",innerError)
//             return apiResponse.validationErrorWithData(
//               res,
//               `Internal Server Error`,
//               ""
//             );
//           });
//         }
//         console.log("end of API")
//     } catch (err) {
//       console.log(err);
//       //throw error in json response with status 500.
//       return apiResponse.ErrorResponse(res, err);
//     }
//   },
// ];


exports.BankOTPSubmit = [
  // auth,
  // accessValidation
  body("otp", "OTP Number must not be empty.")
    .isLength({ min: 1 })
    .trim(),
  sanitizeBody("otp").escape(),
  async (req, res) => {
    try {
      console.log(req.body);
      const errors = validationResult(req);
      if (!errors.isEmpty()) {
        return apiResponse.validationErrorWithData(
          res,
          "Validation Error.",
          errors.array()
        );
      }
      const bankAPIRes = await BankAPIResponseModel.find({order:{$in:req.body.order}});
      let bankAPIDoc= bankAPIRes[bankAPIRes.length-1]
      console.log("fetched order", bankAPIDoc);
      let PaymentDetails = {
        app_id:"2452014243",
        app_key:"24490245",
        authToken: bankAPIDoc.authToken,
        hashKey: bankAPIDoc.hashKey,
        otp: req.body.otp,
        transactionReferenceNumber: bankAPIDoc.transactionReferenceNumber,
        transactionTypeId: bankAPIDoc.transactionTypeId,
        isOtp: bankAPIDoc.isOtp
      };
      const agent = new https.Agent({
        rejectUnauthorized: false,
      });
      const ConnectDot = await axios.post(
        "https://srv.connectdotnet.com/WebSrv/api/MMPayment/ProcessPayment",
        PaymentDetails,
        { httpsAgent: agent }
      ).then((axiosRes)=>{
        console.log("should wait ",axiosRes.data)
        const ConnectDotRes= axiosRes.data;
        if (ConnectDotRes.status_code === 0) {
            console.log("error working")
            return apiResponse.validationErrorWithData(
                res,
                `Validation Error, ${ConnectDotRes.status_message}`,
                ""
            );
        } else {
          if (ConnectDotRes.status_code === 1) {
            console.log(" working")
            OrderModel.updateMany(
              {_id:{$in:req.body.order}},
              { payment_status: true, $set: {mobile: req.body.clientAccountNumber} },
              {},
              function (err) {
                if (err) {
                  return apiResponse.ErrorResponse(res, err);
                } else {
                    console.log("working order")
                  return apiResponse.successResponseWithData(
                    res,
                    "payment success",
                    ConnectDotRes
                  );
                }
              }
            );
          }
        }
      }).catch((innerError)=>{
        console.log("inner error working",innerError)
        return apiResponse.validationErrorWithData(
          res,
          `Internal Server Error`,
          ""
        );
      });
      console.log("end of API")
    }catch(err) {
      console.log(err);
      //throw error in json response with status 500.
      return apiResponse.ErrorResponse(res, err);
    }
  },
];


// exports.paymentStore = [
//     body("paymentMode", "Restaurant must not be empty.")
//         .isLength({ min: 1 })
//         .trim(),
//       body("clientAccountNumber", "Account Number must not be empty.")
//         .isLength({ min: 1 })
//         .trim(),
//       sanitizeBody("paymentMode").escape(),
//       sanitizeBody("clientAccountNumber").escape(),
//     async (req, res) => {
//         const errors = validationResult(req);
//             if (!errors.isEmpty()) {
//             return apiResponse.validationErrorWithData(
//                 res,
//                 "Validation Error.",
//                 errors.array()
//             );
//         }   
//         try {
//         console.log(req.body);
//         const orderDetails = await OrderModel.findById(req.body.order);
//         console.log("fetched order", orderDetails);
//         let PaymentDetails = {
//           app_id: "2452014243",
//           app_key: "24490245",
//           name: "ZOHAIB",
//           feeTypeCode: "GENERALPAYMENT",
//           mobile: "+923123456789",
//           currency: "PKR",
//           amount: orderDetails.total_price,
//           // "email":"zohaibshahid915@gmail.com",
//           orderId: req.body.order,
//           // orderDesc:"not a test API",
//           paymentMode: req.body.paymentMode,
//           clientAccountNumber: req.body.clientAccountNumber,
//         };
//         if (req.body.paymentMode === "JAZZCASHWALLET") {
//           PaymentDetails["cnic"] = req.body.cnic;
//         }
//       //   if (PaymentDetails.is_cancel) {
//       //     apiResponse.cascadeErrorWithData(
//       //       res,
//       //       "Invalid Attempt. Order already cancel",
//       //       " "
//       //     );
//       //   } else {
//           const agent = new https.Agent({
//             rejectUnauthorized: false,
//           });
          
//           const ConnectDot = await axios.post(
//               "https://srv.connectdotnet.com/WebSrv/api/MMPayment/Initiate",
//               PaymentDetails,
//               { httpsAgent: agent }
//           ).then((axiosRes)=>{
//               console.log("should wait ",axiosRes.data)
//               const ConnectDotRes= axiosRes.data;
//               if (ConnectDotRes.status_code === 0) {
//                   console.log("error working")
//                   return apiResponse.validationErrorWithData(
//                       res,
//                       `Validation Error, ${ConnectDotRes.status_message}`,
//                       ""
//                   );
//               } else {
//                 if (ConnectDotRes.status_code === 1) {
//                   console.log(" working")
//                   // OrderModel.findByIdAndUpdate(
//                   //   req.params.id,
//                   //   { payment_status: true },
//                   //   {},
//                   //   function (err) {
//                   //     if (err) {
//                   //       return apiResponse.ErrorResponse(res, err);
//                   //     } else {
//                         return apiResponse.successResponseWithData(
//                           res,
//                           "payment success",
//                           []
//                         );
//                   //     }
//                   //   }
//                   // );
//                 }
//               }
  
//           }).catch((innerError)=>{
//               console.log("inner error working",innerError)
//                   // return apiResponse.validationErrorWithData(
//                   //     res,
//                   //     `Validation Error, ${ConnectDotRes.status_message}`,
//                   //     ""
//                   // );
//           });
//           console.log("end of API")
//       } catch (err) {
//         console.log(err);
//         //throw error in json response with status 500.
//         return apiResponse.ErrorResponse(res, err);
//       }
//     },
//   ];
  