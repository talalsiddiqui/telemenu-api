const Restaurant = require("../models/RestaurantModel");
const apiResponse = require("../helpers/apiResponse");
const auth = require("../middlewares/jwt");
const accessValidation = require("../middlewares/userValidation");
const ItemModel = require("../models/ItemModel");
const ModifierModel = require('./../models/ModifierModel');
const SectionModel = require('./../models/SectionModel');

const LanguageModel = require('./../models/LanguageModel');
const { body,validationResult } = require("express-validator");
const { sanitizeBody } = require("express-validator");
var mongoose = require("mongoose");
mongoose.set("useFindAndModify", false);

exports.languageList = [
	// auth,
	// accessValidation,
	function (req, res) {
		try {
			console.log(req.params.id)
            LanguageModel
            .find({})
            .then((lanDoc)=>{
                if(lanDoc.length > 0){
                    let langJSON = {
                        "en":{
                            "translation": {}
                        }
                        ,
                        "urdu":{
                            "translation": {}
                        }
                    }
                    lanDoc.map((langObj)=>{
                        langJSON[`en`][`translation`][`${langObj.trans_id}`] = langObj.en_name
                        langJSON[`urdu`][`translation`][`${langObj.trans_id}`] = langObj.urdu_name
                    })
                    console.log(langJSON)
                    return res.status(200).json(langJSON); 
                    // apiResponse.successResponseWithData(res, "List fetched success", langJSON);
                }else{
                    return apiResponse.successResponseWithData(res, "No data found", []);
                }
            });           
		} catch (err) { 
			return apiResponse.ErrorResponse(res, err);
		}
	}
];


/**
 * Category store.
 * 
 * @param {string}      title 
 * @param {string}      description
 * @param {string}      isbn
 * 
 * @returns {Object}
 */
exports.languageStore = [
	auth,
	accessValidation,
	body("type", "Type must not be empty.").isLength({ min: 1 }).trim(),
	body("urdu_name", "Urdu Name must not be empty.").isLength({ min: 1 }).trim(),
	body("restaurant", "Restaurant must not be Select.")
    .isLength({ min: 1 }).trim()
    .custom((value) => {
        return Restaurant.findById(value).then((restaurant) => {
          if (!restaurant) {
            return Promise.reject("Restaurant not Exist");
          }
        });
    }),
	sanitizeBody("*").escape(),
	async(req, res) => {
		try {
            console.log(req.body)
            const errors = validationResult(req);
            var TransID,TransName;
            if(req.body.type==="item"){
                await ItemModel.findById(req.body.item).then((item)=>{
                    if(!item) return apiResponse.notFoundResponse(res,"Item not exists with this id");
                    console.log("transId Exist",item.trans_id)
                    TransName = item.name
                    TransID = item.trans_id
                })
            }else if(req.body.type==="modifier"){
                await ModifierModel.findById(req.body.modifier).then((modifier)=>{
                    if(!modifier) return apiResponse.notFoundResponse(res,"Item not exists with this id");
                    TransName = modifier.name
                    TransID = modifier.trans_id
                })
            }else if(req.body.type==="section"){
                await SectionModel.findById(req.body.section).then((section)=>{
                    if(!section) return apiResponse.notFoundResponse(res,"Item not exists with this id");
                    TransName = section.name
                    TransID = section.trans_id
                })
            }else{
                return apiResponse.notFoundResponse(res,"Invalid Type");
            }
            console.log("working 1")
            var lang = new LanguageModel({   
                restaurant: req.body.restaurant,
                trans_id: TransID,
                en_name: TransName,
                urdu_name: req.body.urdu_name
            });
            if(!errors.isEmpty()) {
                return apiResponse.validationErrorWithData(res, "Validation Error.", errors.array());
			}
			else{
                LanguageModel.findOne({restaurant:req.body.restaurant,trans_id:TransID}).then((langFound) => {
                    if (!langFound) {
                        lang.save(function (err,langaugeDoc) {
                            if (err) return apiResponse.ErrorResponse(res, err); 
                            return apiResponse.successResponseWithData(res, "Translate add success.", []);
                        });
                    }else{
                        LanguageModel.findByIdAndUpdate(langFound._id, {urdu_name: req.body.urdu_name}, {},function (err) {
                            if (err) return apiResponse.ErrorResponse(res, err);
                            return apiResponse.successResponseWithData(res, "Translate update success.", []);
                        })
                    }
                });
                
            }
        } catch (err) {
			//throw error in json response with status 500. 
			return apiResponse.ErrorResponse(res, err);
		}
	}
];