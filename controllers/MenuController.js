const MenuModel = require("../models/MenuModel"); 
const Restaurant = require("../models/RestaurantModel");
const { body,validationResult } = require("express-validator");
const { sanitizeBody } = require("express-validator");
const apiResponse = require("../helpers/apiResponse");
const auth = require("../middlewares/jwt");
const accessValidation = require("../middlewares/userValidation");
var mongoose = require("mongoose");
mongoose.set("useFindAndModify", false);

/**
 * Menu List.
 * 
 * @returns {Object}
 */


exports.menuList = [
	auth,
	accessValidation,
	function (req, res) {
		try {
			console.log(req.user._id)
			if (req.user.user_type ==="owner"){
				Restaurant
				.find({owner: req.user._id},'_id owner')
				.then((restaurantList)=>{
					if(restaurantList === null){
						return apiResponse.notFoundResponse(res,"Restaurant not exists with this id");
					}else{
						restaurantIDs = restaurantList.map((singleRestaurant)=>{
							return singleRestaurant._id
						})
						MenuModel
						.find({restaurant: {$in:restaurantIDs}}, "_id name restaurant description order_message busy_message is_busy sections")
						.populate([{
							path: 'restaurant',
							select: '_id name'
						}])
						.populate('sections')
						.then((menus)=>{
							if(menus.length > 0){
								return apiResponse.successResponseWithData(res, "List fetched success", menus);
							}else{
								return apiResponse.successResponseWithData(res, "No data found", []);
							}
						});
					}
				})
			}else{
				MenuModel
				.find({},"_id name restaurant description order_message busy_message is_busy sections")
				.populate('restaurant','name')
				.populate('sections')
				.then((menus)=>{
					if(menus.length > 0){
						return apiResponse.successResponseWithData(res, "List fetched success", menus);
					}else{
						return apiResponse.successResponseWithData(res, "No data found", []);
					}
				});
			}        
		} catch (err) { 
			return apiResponse.ErrorResponse(res, err);
		}
	}
];

/**
 * Menu store.
 * 
 * @param {string}      title 
 * @param {string}      description
 * @param {string}      isbn
 * 
 * @returns {Object}
 */
exports.menuStore = [
	auth,
	accessValidation,
	body("name", "Name must not be empty.").isLength({ min: 1 }).trim(),
	body("order_message", "Order Message must not be empty.").isLength({ min: 1 }).trim(),
	body("busy_message", "Busy Message must not be empty.").isLength({ min: 1 }).trim(),
	// body("is_busy", "Is Busy must not be unchecked.").isLength({ min: 1 }).trim(),
	body("restaurant", "Restaurant must not be Select.")
	.isLength({ min: 1 })
	.trim()
	.custom((value) => {
		return MenuModel.findOne({ restaurant: value }).then((menu) => {
		  if (menu) {
			return Promise.reject("Restauant menu already define");
		  }
		});
	  })
	,
	sanitizeBody("name").escape(),
	sanitizeBody("order_message").escape(),
	sanitizeBody("busy_message").escape(),
	// sanitizeBody("is_busy").escape(),
	sanitizeBody("restaurant").escape(),
	(req, res) => {
		try {
			console.log(req.body)
            const errors = validationResult(req);
			var menu = new MenuModel(
				{   
                    name: req.body.name,
					description: req.body.description,
					order_message: req.body.order_message,
					busy_message: req.body.busy_message,
					is_busy: req.body.is_busy,
					restaurant: req.body.restaurant,
					sections: req.body.sections,
                });
			if (!errors.isEmpty()) {
				return apiResponse.validationErrorWithData(res, "Validation Error.", errors.array());
			}
			else {             
                //Save Menu.
				menu.save(function (err,menuDoc) {
					if (err)  return apiResponse.ErrorResponse(res, err); 
					console.log("controller end",menuDoc)

					if (req.user.user_type ==="owner"){
						Restaurant
						.find({owner: req.user._id},'_id owner')
						.then((restaurantList)=>{
							if(restaurantList === null){
								return apiResponse.notFoundResponse(res,"Restaurant not exists with this id");
							}else{
								restaurantIDs = restaurantList.map((singleRestaurant)=>{
									return singleRestaurant._id
								})
								MenuModel
								.find({restaurant: {$in:restaurantIDs}}, "_id name restaurant description order_message busy_message is_busy sections")
								.populate('restaurant','_id name')
								.populate('sections')
								.then((menuDOCs)=>{
									if(menuDOCs.length > 0){
										return apiResponse.successResponseWithData(res, "Menu add success", menuDOCs);
									}else{
										return apiResponse.successResponseWithData(res, "Menu add success", []);
									}
								});
							}
						})
					}else{
						MenuModel
						.find({},"_id name restaurant description order_message busy_message is_busy sections")
						.populate('restaurant','name')
						.populate('sections')
						.then((menuDOCs)=>{
							if(menuDOCs.length > 0){
								return apiResponse.successResponseWithData(res, "Menu add success", menuDOCs);
							}else{
								return apiResponse.successResponseWithData(res, "Menu add success", []);
							}
						});
					}
				});
			}
		} catch (err) {
			//throw error in json response with status 500. 
			return apiResponse.ErrorResponse(res, err);
		}
	}
];

/**
 * Menu update.
 * 
 * @param {string}      title 
 * @param {string}      description
 * @param {string}      isbn
 * 
 * @returns {Object}
 */
exports.menuUpdate = [
	auth,
	accessValidation,
	body("name", "Name must not be empty.").isLength({ min: 1 }).trim(),
	body("order_message", "Order Message must not be empty.").isLength({ min: 1 }).trim(),
	body("busy_message", "Busy Message must not be empty.").isLength({ min: 1 }).trim(),
	// body("is_busy", "Price must not be empty.").isLength({ min: 1 }).trim(),
	body("restaurant", "Restaurant must not be Select.")
	.isLength({ min: 1 })
	.trim()
	.custom((value) => {
		return MenuModel.find({ restaurant: value }).then((menu) => {
		  if (menu) {
			if(menu.length>1){
			  return Promise.reject("Restauant menu already define");
			}
		  }
		});
	  }),
	sanitizeBody("name").escape(),
	sanitizeBody("order_message").escape(),
	sanitizeBody("busy_message").escape(),
	// sanitizeBody("is_busy").escape(),
	sanitizeBody("restaurant").escape(),
	(req, res) => {
		try {
			const errors = validationResult(req);
			console.log(req.body)
			var menu = new MenuModel(
                {   
                    name: req.body.name,
					description: req.body.description,
					order_message: req.body.order_message,
					busy_message: req.body.busy_message,
					is_busy: req.body.is_busy,
					restaurant: req.body.restaurant,
					sections: req.body.sections,
					_id:req.params.id
				});
			if (!errors.isEmpty()) {
				return apiResponse.validationErrorWithData(res, "Validation Error.", errors.array());
			}
			else {
				if(!mongoose.Types.ObjectId.isValid(req.params.id)){
					return apiResponse.validationErrorWithData(res, "Invalid Error.", "Invalid Menu ID");
				}else{
					MenuModel.findById(req.params.id, function (err, foundMenu) {
						if(foundMenu === null){
							return apiResponse.notFoundResponse(res,"Menu not exists with this id");
						}else{
							//update Menu.
							MenuModel.findByIdAndUpdate(req.params.id, menu, {},function (err) {
								if (err) { 
									return apiResponse.ErrorResponse(res, err); 
								}else{
									if (req.user.user_type ==="owner"){
										Restaurant
										.find({owner: req.user._id},'_id owner')
										.then((restaurantList)=>{
											if(restaurantList === null){
												return apiResponse.notFoundResponse(res,"Restaurant not exists with this id");
											}else{
												restaurantIDs = restaurantList.map((singleRestaurant)=>{
													return singleRestaurant._id
												})
												MenuModel
												.find({restaurant: {$in:restaurantIDs}}, "_id name restaurant description order_message busy_message is_busy sections")
												.populate('restaurant','_id name')
												.populate('sections')
												.then((menuDOCs)=>{
													if(menuDOCs.length > 0){
														return apiResponse.successResponseWithData(res, "Menu Update success", menuDOCs);
													}else{
														return apiResponse.successResponseWithData(res, "Menu Update success", []);
													}
												});
											}
										})
									}else{
										MenuModel
										.find({},"_id name restaurant description order_message busy_message is_busy sections")
										.populate('restaurant','name')
										.populate('sections')
										.then((menuDOCs)=>{
											if(menuDOCs.length > 0){
												return apiResponse.successResponseWithData(res, "Menu Update success", menuDOCs);
											}else{
												return apiResponse.successResponseWithData(res, "Menu Update success", []);
											}
										});
									}
								}
							});
						}
					});
				}
			}
		} catch (err) {
			//throw error in json response with status 500. 
			return apiResponse.ErrorResponse(res, err);
		}
	}
];

/**
 * Category Delete.
 * 
 * @param {string}      id
 * 
 * @returns {Object}
 */
exports.menuDelete = [
	auth,
	accessValidation,
	function (req, res) {
		console.log(req.params)
		if(!mongoose.Types.ObjectId.isValid(req.params.id)){
			return apiResponse.validationErrorWithData(res, "Invalid Error.", "Invalid ID");
		}
		try {
			MenuModel.findById(req.params.id, function (err, foundMenu) {
				if(foundMenu === null){
					return apiResponse.notFoundResponse(res,"Modifier not exists with this id ");
				}else{
					//delete Menu.
					MenuModel.findByIdAndRemove(req.params.id,function (err) {
						if (err) { 
							return apiResponse.ErrorResponse(res, err); 
						}else{
							if (req.user.user_type ==="owner"){
								Restaurant
								.find({owner: req.user._id},'_id owner')
								.then((restaurantList)=>{
									if(restaurantList === null){
										return apiResponse.notFoundResponse(res,"Restaurant not exists with this id");
									}else{
										restaurantIDs = restaurantList.map((singleRestaurant)=>{
											return singleRestaurant._id
										})
										MenuModel
										.find({restaurant: {$in:restaurantIDs}}, "_id name restaurant description order_message busy_message is_busy sections")
										.populate('restaurant','_id name')
										.populate('sections')
										.then((menuDOCs)=>{
											if(menuDOCs.length > 0){
												return apiResponse.successResponseWithData(res, "Menu delete success", menuDOCs);
											}else{
												return apiResponse.successResponseWithData(res, "Menu delete success", []);
											}
										});
									}
								})
							}else{
								MenuModel
								.find({},"_id name restaurant description order_message busy_message is_busy sections")
								.populate('restaurant','name')
								.populate('sections')
								.then((menuDOCs)=>{
									if(menuDOCs.length > 0){
										return apiResponse.successResponseWithData(res, "Menu delete success", menuDOCs);
									}else{
										return apiResponse.successResponseWithData(res, "Menu delete success", []);
									}
								});
							}
						}
					});
				}
			});
		} catch (err) {
			//throw error in json response with status 500. 
			return apiResponse.ErrorResponse(res, err);
		}
	}
];


exports.specificMenuListForUserDisplay = [
	function (req, res) {
		if(!mongoose.Types.ObjectId.isValid(req.params.id)){
			return apiResponse.validationErrorWithData(res, "Invalid Restaurant ID", "");
		}
		try {
			MenuModel
			.find({restaurant: req.params.id}, "_id name sections restaurant ")
			.populate('sections')
			.populate('restaurant')
			.then((menus)=>{
				if(menus.length > 0){
					// ItemModel.find({ restaurant: req.params.id })
					// .sort({sell_count: 1})
					// .limit(10)
					// .populate('restaurant')
					// .then((top_Selling_items) => {
					//   if (top_Selling_items.length > 0) {
					// 	const Menu= [];
					// 	Menu.push(
					// 		{
					// 		  restaurant: gotSpecificMenuListData[0].restaurant,
					// 		  sections: [
					// 			...gotSpecificMenuListData[0].sections,
					// 			{
					// 			  name: 'Top Selling',
					// 			  urdu_name:'زیادہ فروخت',
					// 			  description: '',
					// 			  restaurant: prop.match.params.Rid,
					// 			  is_parent: false,
					// 			  items: TopSelling,
					// 			  trans_id:`top_selling_123`,
					// 			  custom_schedule:false,
					// 			}
					// 		  ]
					// 		}
					// 	  )
					// 	return apiResponse.successResponseWithData(
					// 	  res,
					// 	  "List fetched success",
					// 	  top_Selling_items
					// 	);
					//   } else {
					// 	return apiResponse.successResponseWithData(
					// 	  res,
					// 	  "No data found",
					// 	  []
					// 	);
					//   }
					// });

					return apiResponse.successResponseWithData(res, "List fetched success", menus);
				}else{
					return apiResponse.successResponseWithData(res, "No data found", []);
				}
			});        
		} catch (err) { 
			return apiResponse.ErrorResponse(res, err);
		}
	}
];
