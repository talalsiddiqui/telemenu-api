const SectionModel = require("../models/SectionModel"); 
const Restaurant = require("../models/RestaurantModel");
const ItemModel = require("../models/ItemModel");
const LanguageModel = require("../models/LanguageModel");
const { body,validationResult } = require("express-validator");
const { sanitizeBody } = require("express-validator");
const apiResponse = require("../helpers/apiResponse");
const auth = require("../middlewares/jwt");
const accessValidation = require("../middlewares/userValidation");
var mongoose = require("mongoose");
mongoose.set("useFindAndModify", false);
const { uid } = require('uid');


// exports.sectionList = [
// 	auth,
// 	accessValidation,
// 	function (req, res) {
// 		try {
// 			console.log(req.user._id)
// 			if (req.user.user_type ==="owner"){
// 				Restaurant
// 				.find({owner: req.user._id},'_id owner')
// 				.then((restaurantList)=>{
// 					if(restaurantList === null){
// 						return apiResponse.notFoundResponse(res,"Restaurant not exists with this id");
// 					}else{
// 						restaurantIDs = restaurantList.map((singleRestaurant)=>{
// 							return singleRestaurant._id
// 						})
// 						SectionModel
// 						.find({restaurant: {$in:restaurantIDs}, is_parent:true }, "_id name description restaurant items parent")
// 						.populate('restaurant','_id name')
// 						.populate('parent','_id name')
// 						.then((sections)=>{
// 							if(sections.length > 0){
// 								return apiResponse.successResponseWithData(res, "Parent sections Fetched success", sections);
// 							}else{
// 								return apiResponse.successResponseWithData(res, "No data found", []);
// 							}
// 						});
// 					}
// 				})
// 			}else{
// 				SectionModel
// 				.find({is_parent:true},"_id name description restaurant items parent")
// 				.populate('restaurant','_id name')
// 				.populate('parent','_id name')
// 				.then((sections)=>{
// 					if(sections.length > 0){
// 						return apiResponse.successResponseWithData(res, "Parent sections Fetched success", sections);
// 					}else{
// 						return apiResponse.successResponseWithData(res, "No data found", []);
// 					}
// 				});
// 			}        
// 		} catch (err) { 
// 			return apiResponse.ErrorResponse(res, err);
// 		}
// 	}
// ];



exports.specificRestaurantSectionList = [
	auth,
	accessValidation,
	function (req, res) {
		try {
            console.log(req.params.id)
			SectionModel.find({restaurant: req.params.id},"_id name").then((sections)=>{
				console.log(sections)
				if(sections.length > 0){
					return apiResponse.successResponseWithData(res, "List fetched", sections);
				}else{
					return apiResponse.successResponseWithData(res, "No data found", []);
				}
			});
		} catch (err) { 
			return apiResponse.ErrorResponse(res, err);
		}
	}
];

/**
 * Section List.
 * 
 * @returns {Object}
 */
exports.sectionList = [
	auth,
	accessValidation,
	function (req, res) {
		try {
			console.log(req.user._id)
			if (req.user.user_type ==="owner"){
				Restaurant
				.find({owner: req.user._id},'_id owner')
				.then((restaurantList)=>{
					if(restaurantList === null){
						return apiResponse.notFoundResponse(res,"Restaurant not exists with this id");
					}else{
						restaurantIDs = restaurantList.map((singleRestaurant)=>{
							return singleRestaurant._id
						})
						SectionModel
						.find({restaurant: {$in:restaurantIDs}})
						.populate('restaurant','_id name')
						.populate('items','_id name')
						.populate('parent','_id name')
						.then((sections)=>{
							if(sections.length > 0){
								return apiResponse.successResponseWithData(res, "Sections Fetched success", sections);
							}else{
								return apiResponse.successResponseWithData(res, "No data found", []);
							}
						});
					}
				})
			}else{
				SectionModel
				.find({})
				.populate('restaurant','_id name')
				.populate('items','_id name')
				.populate('parent','_id name')
				.then((sections)=>{
					if(sections.length > 0){
						return apiResponse.successResponseWithData(res, "Sections Fetched success", sections);
					}else{
						return apiResponse.successResponseWithData(res, "No data found", []);
					}
				});
			}        
		} catch (err) { 
			return apiResponse.ErrorResponse(res, err);
		}
	}
];

exports.subSectionList = [
	auth,
	accessValidation,
	function (req, res) {
		try {
			console.log(req.user._id)
			if (req.user.user_type ==="owner"){
				Restaurant
				.find({owner: req.user._id},'_id owner')
				.then((restaurantList)=>{
					if(restaurantList === null){
						return apiResponse.notFoundResponse(res,"Restaurant not exists with this id");
					}else{
						restaurantIDs = restaurantList.map((singleRestaurant)=>{
							return singleRestaurant._id
						})
						SectionModel
						.find({restaurant: {$in:restaurantIDs}, is_parent: false}, "_id name description restaurant items parent")
						.populate('restaurant','_id name')
						.populate('items','_id name')
						.populate('parent','_id name')
						.then((sections)=>{
							if(sections.length > 0){
								return apiResponse.successResponseWithData(res, "Sections Fetched success", sections);
							}else{
								return apiResponse.successResponseWithData(res, "No data found", []);
							}
						});
					}
				})
			}else{
				SectionModel
				.find({is_parent: false},"_id name description restaurant items parent")
				.populate('restaurant','_id name')
				.populate('items','_id name')
				.populate('parent','_id name')
				.then((sections)=>{
					if(sections.length > 0){
						return apiResponse.successResponseWithData(res, "Sections Fetched success", sections);
					}else{
						return apiResponse.successResponseWithData(res, "No data found", []);
					}
				});
			}        
		} catch (err) { 
			return apiResponse.ErrorResponse(res, err);
		}
	}
];


exports.superSectionList = [
	auth,
	accessValidation,
	function (req, res) {
		try {
			console.log(req.user._id)
			if (req.user.user_type ==="owner"){
				Restaurant
				.find({owner: req.user._id},'_id owner')
				.then((restaurantList)=>{
					if(restaurantList === null){
						return apiResponse.notFoundResponse(res,"Restaurant not exists with this id");
					}else{
						restaurantIDs = restaurantList.map((singleRestaurant)=>{
							return singleRestaurant._id
						})
						SectionModel
						.find({restaurant: {$in:restaurantIDs}, is_parent: true}, "_id name description restaurant items parent")
						.populate('restaurant','_id name')
						.populate('items','_id name')
						.populate('parent','_id name')
						.then((sections)=>{
							if(sections.length > 0){
								return apiResponse.successResponseWithData(res, "Sections Fetched success", sections);
							}else{
								return apiResponse.successResponseWithData(res, "No data found", []);
							}
						});
					}
				})
			}else{
				SectionModel
				.find({is_parent: true},"_id name description restaurant items parent")
				.populate('restaurant','_id name')
				.populate('items','_id name')
				.populate('parent','_id name')
				.then((sections)=>{
					if(sections.length > 0){
						return apiResponse.successResponseWithData(res, "Sections Fetched success", sections);
					}else{
						return apiResponse.successResponseWithData(res, "No data found", []);
					}
				});
			}        
		} catch (err) { 
			return apiResponse.ErrorResponse(res, err);
		}
	}
];
/**
 * Section store.
 * 
 * @param {string}      title 
 * @param {string}      description
 * @param {string}      isbn
 * 
 * @returns {Object}
 */
exports.sectionStore = [
	auth,
	accessValidation,
	body("name", "Name must not be empty.").isLength({ min: 1 }).trim(),
	body("restaurant", "Restaurant must not be Select.").isLength({ min: 1 }).trim(),
	body("custom_schedule", "Schedule must not be empty.").isLength({ min: 1 }).trim(),
	sanitizeBody("name").escape(),
	sanitizeBody("restaurant").escape(),
	sanitizeBody("custom_schedule").escape(),
	(req, res) => {
		try {
			console.log(req.body)
            const errors = validationResult(req);
			let section;
			var TransID = req.body.name.trim()
			TransID = `${TransID.replace(/ /g,'_')}_${uid(10)}`
			if(req.body.items ){
				if(req.body.parent){
					section = new SectionModel({   
						name: req.body.name,
						urdu_name: req.body.urdu_name,
						description: req.body.description,
						restaurant: req.body.restaurant,
						items: req.body.items,
						parent: req.body.parent,
						trans_id: TransID,
						is_parent: false,
						custom_schedule: req.body.custom_schedule,
						start_time: req.body.start_time,
						end_time: req.body.end_time,	
					});
				}else{
					section = new SectionModel({   
						name: req.body.name,
						urdu_name: req.body.urdu_name,
						description: req.body.description,
						restaurant: req.body.restaurant,
						items: req.body.items,
						trans_id: TransID,
						is_parent: false,
						custom_schedule: req.body.custom_schedule,
						start_time: req.body.start_time,
						end_time: req.body.end_time,
					});
				}
			}else{
				section= new SectionModel({   
					name: req.body.name,
					urdu_name: req.body.urdu_name,
					description: req.body.description,
					restaurant: req.body.restaurant,
					trans_id: TransID,
					is_parent: true,
					custom_schedule: req.body.custom_schedule,
					start_time: req.body.start_time,
					end_time: req.body.end_time,
				});
			}
			console.log("section create working fine")
			if (!errors.isEmpty()) {
				return apiResponse.validationErrorWithData(res, "Validation Error.", errors.array());
			}
			else {
                //Save Section.
				section.save(function (err,sectionDoc) {
					if (err)  return apiResponse.ErrorResponse(res, err); 
					console.log("controller end",sectionDoc)
					var lang = new LanguageModel({   
						restaurant: sectionDoc.restaurant,
						trans_id: sectionDoc.trans_id,
						en_name: sectionDoc.name,
						urdu_name: sectionDoc.urdu_name ? sectionDoc.urdu_name: sectionDoc.name
					});
					lang.save(function (err,langaugeDoc) {
						if (err) return apiResponse.ErrorResponse(res, err);
						if (req.user.user_type ==="owner"){
							Restaurant
							.find({owner: req.user._id},'_id owner')
							.then((restaurantList)=>{
								if(restaurantList === null){
									return apiResponse.notFoundResponse(res,"Restaurant not exists with this id");
								}else{
									restaurantIDs = restaurantList.map((singleRestaurant)=>{
										return singleRestaurant._id
									})
									SectionModel
									.find({restaurant: {$in:restaurantIDs}})
									.populate('restaurant','_id name')
									.populate('items','_id name')
									.populate('parent','_id name')
									.then((sections)=>{
										if(sections.length > 0){
											return apiResponse.successResponseWithData(res, "Section add success", sections);
										}else{
											return apiResponse.successResponseWithData(res, "Section add success", []);
										}
									});
								}
							})
						}else{
							SectionModel
							.find({})
							.populate('restaurant','_id name')
							.populate('items','_id name')
							.populate('parent','_id name')
							.then((sections)=>{
								if(sections.length > 0){
									return apiResponse.successResponseWithData(res, "Section add success", sections);
								}else{
									return apiResponse.successResponseWithData(res, "Section add success", []);
								}
							});
						}
					})
				});
			}
		} catch (err) {
			//throw error in json response with status 500. 
			return apiResponse.ErrorResponse(res, err);
		}
	}
];

/**
 * Section update.
 * 
 * @param {string}      title 
 * @param {string}      description
 * @param {string}      isbn
 * 
 * @returns {Object}
 */
exports.sectionUpdate = [
	auth,
	accessValidation,
	body("name", "Name must not be empty.").isLength({ min: 1 }).trim(),
	body("restaurant", "Restaurant must not be Select.").isLength({ min: 1 }).trim(),
	body("custom_schedule", "Schedule must not be empty.").isLength({ min: 1 }).trim(),
	sanitizeBody("name").escape(),
	sanitizeBody("restaurant").escape(),
	sanitizeBody("custom_schedule").escape(),
	(req, res) => {
		try {
			const errors = validationResult(req);
			console.log(req.body)
			if (!errors.isEmpty()) {
				return apiResponse.validationErrorWithData(res, "Validation Error.", errors.array());
			}
			else {
				if(!mongoose.Types.ObjectId.isValid(req.params.id)){
					return apiResponse.validationErrorWithData(res, "Invalid Error.", "Invalid section ID");
				}else{
					SectionModel.findById(req.params.id, function (err, foundSection) {
						if(foundSection === null){
							return apiResponse.notFoundResponse(res,"Section not exists with this id");
						}else{
							if(req.body.parent && req.body.items){
								if(req.params.id == req.body.parent){
									return apiResponse.validationErrorWithData(res, "Edit section and selected section are same", errors.array());	
								}
							}
							let section= new SectionModel({   
								name: req.body.name,
								urdu_name: req.body.urdu_name,
								description: req.body.description,
								restaurant: req.body.restaurant,
								parent: req.body.parent ? req.body.parent:'',
								items: req.body.items ? req.body.items : [],
								is_parent: req.body.items ? false : true,
								custom_schedule: req.body.custom_schedule,
								start_time: req.body.start_time ? req.body.start_time:new Date(),
								end_time: req.body.end_time ? req.body.end_time:new Date(),
								_id:req.params.id
							});
							SectionModel.findByIdAndUpdate(req.params.id, section, {},function (err,afterUpdateSection) {
								if (err) {
									return apiResponse.ErrorResponse(res, err); 
								}else{
									LanguageModel.findOneAndUpdate(
										{
											restaurant:afterUpdateSection.restaurant,
											trans_id:afterUpdateSection.trans_id
										},{ 
											en_name:req.body.name,
											urdu_name:req.body.urdu_name ? req.body.urdu_name: req.body.name, 
										},{},(err,afterupdate)=>{
										if (err) {
											return apiResponse.ErrorResponse(res, err); 
										}else{
											if (req.user.user_type ==="owner"){
												Restaurant
												.find({owner: req.user._id},'_id owner')
												.then((restaurantList)=>{
													if(restaurantList === null){
														return apiResponse.notFoundResponse(res,"Restaurant not exists with this id");
													}else{
														restaurantIDs = restaurantList.map((singleRestaurant)=>{
															return singleRestaurant._id
														})
														SectionModel
														.find({restaurant: {$in:restaurantIDs}})
														.populate('restaurant','_id name')
														.populate('items','_id name')
														.populate('parent','_id name')
														.then((sections)=>{
															if(sections.length > 0){
																return apiResponse.successResponseWithData(res, "Section Update success", sections);
															}else{
																return apiResponse.successResponseWithData(res, "Section Update success", []);
															}
														});
													}
												})
											}else{
												SectionModel
												.find({})
												.populate('restaurant','_id name')
												.populate('items','_id name')
												.populate('parent','_id name')
												.then((sections)=>{
													if(sections.length > 0){
														return apiResponse.successResponseWithData(res, "Section Update success", sections);
													}else{
														return apiResponse.successResponseWithData(res, "Section Update success", []);
													}
												});
											}
										}
									})
									
								}
							});
						}
					});
				}
			}
		} catch (err) {
			//throw error in json response with status 500. 
			return apiResponse.ErrorResponse(res, err);
		}
	}
];

/**
 * Section Delete.
 * 
 * @param {string}      id
 * 
 * @returns {Object}
 */
exports.sectionDelete = [
	auth,
	accessValidation,
	function (req, res) {
		console.log(req.params)
		if(!mongoose.Types.ObjectId.isValid(req.params.id)){
			return apiResponse.validationErrorWithData(res, "Invalid Error.", "Invalid ID");
		}
		try {
			SectionModel.findById(req.params.id, function (err, foundSection) {
				if(foundSection === null){
					return apiResponse.notFoundResponse(res,"Section not exists with this id ");
				}else{
					//delete Section
					SectionModel.findByIdAndRemove(req.params.id,function (err) {
						if (err) { 
							return apiResponse.ErrorResponse(res, err); 
						}else{
							if (req.user.user_type ==="owner"){
								Restaurant
								.find({owner: req.user._id},'_id owner')
								.then((restaurantList)=>{
									if(restaurantList === null){
										return apiResponse.notFoundResponse(res,"Restaurant not exists with this id");
									}else{
										restaurantIDs = restaurantList.map((singleRestaurant)=>{
											return singleRestaurant._id
										})
										SectionModel
										.find({restaurant: {$in:restaurantIDs}})
										.populate('restaurant','_id name')
										.populate('items','_id name')
										.populate('parent','_id name')
										.then((sections)=>{
											if(sections.length > 0){
												return apiResponse.successResponseWithData(res, "Section Delete success", sections);
											}else{
												return apiResponse.successResponseWithData(res, "Section Delete success", []);
											}
										});
									}
								})
							}else{
								SectionModel
								.find({})
								.populate('restaurant','_id name')
								.populate('items','_id name')
								.populate('parent','_id name')
								.then((sections)=>{
									if(sections.length > 0){
										return apiResponse.successResponseWithData(res, "Section Delete success", sections);
									}else{
										return apiResponse.successResponseWithData(res, "Section Delete success", []);
									}
								});
							}
						}
					});
				}
			});
		} catch (err) {
			//throw error in json response with status 500. 
			return apiResponse.ErrorResponse(res, err);
		}
	}
];

/**
 * Specific Item and sections List.
 * 
 * @returns {Object}
 */

exports.specificItemandSectionList = [
	auth,
	accessValidation,
	function (req, res) {
		try {
			console.log(req.user._id)
			ItemModel
			.find({restaurant: req.params.id,active:true}, "_id name")
			.then((items)=>{
				if(items.length > 0){
					SectionModel
					.find({restaurant: req.params.id,is_parent:true}, "_id name")
					.then((sections)=>{
						if(sections.length > 0){
							return apiResponse.successResponseWithData(res, "List fetched success", {items,sections});
						}else{
							return apiResponse.successResponseWithData(res, "No data found", {items});
						}
					});
				}else{
					return apiResponse.successResponseWithData(res, "No data found", []);
				}
			});
		} catch (err) { 
			return apiResponse.ErrorResponse(res, err);
		}
	}
];




exports.getSectionById = [
	// auth,
	// accessValidation,
	function (req, res) {
		if(!mongoose.Types.ObjectId.isValid(req.params.id)){
			return apiResponse.validationErrorWithData(res, "Invalid Error.", "Invalid ID");
		}
		try {
			SectionModel.findById(req.params.id, function (err, foundSection) {
				if(foundSection === null){
					return apiResponse.notFoundResponse(res,"Section not exists with this id");
				}else{
					let searchObject = {};
					if(foundSection.is_parent){
						searchObject = {
							parent: foundSection._id
						}
						// SectionModel
						// .find({parent: foundSection._id})
						// .populate('items')
						// .then((sections)=>{
						// 	if(sections.length > 0){
						// 		return apiResponse.successResponseWithData(res, "List fetched success", {items,sections});
						// 	}else{
						// 		return apiResponse.successResponseWithData(res, "No data found", []);
						// 	}
						// });	
					}else{
						searchObject = {
							_id: foundSection._id
						}
						// SectionModel
						// .find({_id: foundSection._id})
						// .populate('items')
						// .then((sections)=>{
						// 	if(sections.length > 0){
						// 		return apiResponse.successResponseWithData(res, "List fetched success", {items,sections});
						// 	}else{
						// 		return apiResponse.successResponseWithData(res, "No data found", []);
						// 	}
						// });	
					}
					SectionModel
					.find(searchObject)
					.populate('items')
					.populate([{ 
						path: 'items',
						populate: {
						  path: 'restaurant',
						  select: '_id readonly name'
						} 
					 }])
					.then((sections)=>{
						if(sections.length > 0){
							return apiResponse.successResponseWithData(res, "List fetched success", sections);
						}else{
							return apiResponse.successResponseWithData(res, "No data found", []);
						}
					});
				}
			})
		} catch (err) { 
			return apiResponse.ErrorResponse(res, err);
		}
	}
];