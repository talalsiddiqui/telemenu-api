const OrderModel = require("../models/OrderModel");
const Restaurant = require("../models/RestaurantModel");
const ItemModel = require("../models/ItemModel");
const ModifierModel = require("../models/ModifierModel");

const { body, validationResult } = require("express-validator");
const { sanitizeBody } = require("express-validator");
const apiResponse = require("../helpers/apiResponse");
const auth = require("../middlewares/jwt");
const accessValidation = require("../middlewares/userValidation");
var mongoose = require("mongoose");
const OrderDetailModel = require("../models/OrderDetailModel");
mongoose.set("useFindAndModify", false);
const { uid } = require('uid')


exports.orderStatusList = [
  // auth,
  // accessValidation,
  function (req, res) {
    try {
      console.log(req.body)
      OrderModel.find({ _id:  { $in: req.body.ids } })
      .populate('restaurant','name')
      .then((orders) => {
        console.log(orders)
        if(orders.length > 0) {
          orderIDs = orders.map((order) => {
            return order._id;
          });
          OrderDetailModel.find({ order: { $in: orderIDs } })
            .populate("order")
            .populate("items")
            .then((orderDetails) => {
              if (orderDetails.length > 0) {
                return apiResponse.successResponseWithData(
                  res,
                  "List fetched success",
                  { orders, orderDetails }
                );
              } else {
                return apiResponse.successResponseWithData(
                  res,
                  "No data found",
                  []
                );
              }
            });
        } else {
          return apiResponse.successResponseWithData(res, "No data found", []);
        }
      });
    } catch (err) {
      return apiResponse.ErrorResponse(res, err);
    }
  },
];



/**
 * Order List. for specific restaurant
 *
 * @returns {Object}
 */

exports.orderList = [
  auth,
  accessValidation,
  function (req, res) {
    if (!mongoose.Types.ObjectId.isValid(req.params.id)) {
      return apiResponse.validationErrorWithData(
        res,
        "Invalid Restaurant Id",
        ""
      );
    }
    try {
      OrderModel.find({ restaurant: req.params.id, is_active:true }).then((orders) => {
        if (orders.length > 0) {
          orderIDs = orders.map((order) => {
            return order._id;
          });
          OrderDetailModel.find({ order: { $in: orderIDs } })
            .populate("order")
            .populate("items")
            .then((orderDetails) => {
              if (orderDetails.length > 0) {
                return apiResponse.successResponseWithData(
                  res,
                  "List fetched success",
                  { orders, orderDetails }
                );
              } else {
                return apiResponse.successResponseWithData(
                  res,
                  "No data found",
                  []
                );
              }
            });
        } else {
          return apiResponse.successResponseWithData(res, "No data found", []);
        }
      });
    } catch (err) {
      return apiResponse.ErrorResponse(res, err);
    }
  },
];

/**
 * Category store.
 *
 * @param {string}      title
 * @param {string}      description
 * @param {string}      isbn
 *
 * @returns {Object}
 */
// const frontendData={
// 	customer:"12345678",
// 	items:[
// 		{id:"12345678",qty:"3"},
// 		{id:"12345678",qty:"2"}
// 	],
// 	modifiers:[
// 		{id:"12345678",qty:"3"},
// 	],
// }
exports.orderStore = [
  // auth,
  // accessValidation,
  body("restaurant", "Restaurant must not be empty.")
    .isLength({ min: 1 })
    .trim(),
  sanitizeBody("restaurant").escape(),
  async (req, res) => {
    if (!mongoose.Types.ObjectId.isValid(req.body.restaurant)) {
      return apiResponse.validationErrorWithData(
        res,
        "Invalid Restaurant Id",
        ""
      );
    }
    try {
      console.log(req.body);
      const errors = validationResult(req);
      if (!errors.isEmpty()) {
        return apiResponse.validationErrorWithData(
          res,
          "Validation Error.",
          errors.array()
        );
      } else {
        let OrderDetailObjects = [] || {};
        let OrderQty = 0;
        let OrderTotalPrice = 0;
        console.log("req.body",req.body.items)
        if (req.body.items) {
          const ItemIDs = req.body.items.map((item) => {
            for(let i=0;i<item.qty;i++){
              console.log(item._id)
            }
            return item._id;
          });
          const ItemResponse = await ItemModel.find(
            { _id: { $in: ItemIDs } },
            "_id name price"
          ).then((itemsDoc) => {
            return itemsDoc;
          });
          console.log("with modifier working 1");
          console.log("with modifier working 2");
          for (let i = 0; i < req.body.items.length; i++) {
            reqItems = req.body.items[i];
            for (let j = 0; j < ItemResponse.length; j++) {
              mongoItems = ItemResponse[j];
              if (reqItems._id == mongoItems._id) {
                console.log("with modifier working 3 id match");
                if (reqItems.modifiersData) {
                  console.log("with modifier working 4 has modifier");
                  const ModifierIDs = reqItems.modifiersData.map((modifier) => {
                    return modifier._id;
                  });
                  console.log("modifiers Id", ModifierIDs);
                  console.log("with modifier working 4 before fetch");
                  const ModifierResponse = await ModifierModel.find(
                    { _id: { $in: ModifierIDs } },
                    "_id name price"
                  ).then((modifiersDoc) => {
                    console.log("after error response", modifiersDoc);
                    return modifiersDoc;
                  });
                  console.log(
                    "with modifier working 4 response",
                    ModifierResponse
                  );
                  console.log("with modifier working 4 after fetch");
                  let modifersids = [];
                  let modifiertotalprice = 0;
                  let modifierDataObj = [];
                  reqItems.modifiersData.map((reqModifiers) => {
                    ModifierResponse.map((mongoModifier) => {
                      if (reqModifiers._id == mongoModifier._id) {
                        modifersids.push(mongoModifier._id);
                        modifiertotalprice += mongoModifier.price;
                        modifierDataObj = [
                          ...modifierDataObj,
                          {
                            name: mongoModifier.name,
                            price: mongoModifier.price,
                            _id: mongoModifier._id,
                          },
                        ];
                      }
                    });
                  });
                  OrderQty += reqItems.qty;
                  OrderTotalPrice += mongoItems.price + (modifiertotalprice * reqItems.qty);
                  OrderDetailObjects = [
                    ...OrderDetailObjects,
                    {
                      restaurant: req.body.restaurant,
                      items: mongoItems._id,
                      modifiers: modifersids,
                      modifierData: modifierDataObj,
                      qty: reqItems.qty,
                      unit_price: (mongoItems.price+modifiertotalprice),
                    },
                  ];
                  console.log("has modifier end", OrderDetailObjects);
                } else {
                  console.log("no modifier working 5 has modifier");
                  OrderQty += reqItems.qty;
                  OrderTotalPrice += mongoItems.price * reqItems.qty;
                  OrderDetailObjects = [
                    ...OrderDetailObjects,
                    {
                      restaurant: req.body.restaurant,
                      items: mongoItems._id,
                      qty: reqItems.qty,
                      unit_price: mongoItems.price,
                    },
                  ];
                  console.log("no modifier working 5 end");
                }
              }
            }
          }
          console.log("afterset");
          console.log("working 3");
          let OrderID = uid(6)
          var order = new OrderModel({
            restaurant: req.body.restaurant,
            table_no: req.body.tableno,
            order_date: new Date(),
            order_id:OrderID,
            total_qty: OrderQty,
            total_price: OrderTotalPrice,
          });
          console.log("working 4");
          console.log("working 5");
          order.save(function (err, orderRes) {
            if (err) return apiResponse.ErrorResponse(res, err);
            OrderDetailObjects.map((OrderDetailObject) => {
              OrderDetailObject["order"] = orderRes._id;
            });
            OrderDetailModel.create(
              OrderDetailObjects,
              (err, OrderDeatailRes) => {
                if (err) return apiResponse.ErrorResponse(res, err);
                req.body.items.map((itemID)=>{
                  ItemModel.findByIdAndUpdate(itemID._id, { $inc: { sell_count: +itemID.qty } }, {},function (err) {
                  })
                })
                let ORDER = {
                  _id: orderRes._id
                }
                return apiResponse.successResponseWithData(
                  res,
                  "Order Placed",
                  ORDER
                );
                // ItemModel.updateMany(
                //   {_id: {$in:sellCount} },
                //   { $inc: { sell_count: +1 } },
                //   function (err) {
                //     if (err) {
                //       return apiResponse.ErrorResponse(res, err);
                //     } else {
                      
                //     }
                //   }
                // )
              }
            );
          });
        }
      }
    } catch (err) {
      console.log(err);
      //throw error in json response with status 500.
      return apiResponse.ErrorResponse(res, err);
    }
  },
];

/**
 * Category update.
 *
 * @param {string}      title
 * @param {string}      description
 * @param {string}      isbn
 *
 * @returns {Object}
 */
exports.orderUpdate = [
  auth,
  accessValidation,
  body("restaurant", "Restaurant must not be empty.")
    .isLength({ min: 1 })
    .trim(),
  body("order", "Order ID must not be empty.")
    .isLength({ min: 1 })
    .trim(),
  sanitizeBody("restaurant").escape(),
  sanitizeBody("order").escape(),
  async (req, res) => {
    try {
      console.log(req.body)
      const errors = validationResult(req);
      let OrderDetailObjects = [] || {};
      let OrderQty = 0;
      let OrderTotalPrice = 0;
      OrderModel.findById(req.params.id, function (err, orderFound) {
        if (orderFound === null) {
          return apiResponse.notFoundResponse(
            res,
            "Order not exists with this id"
          );
        } else {
          OrderQty = orderFound.total_qty
          OrderTotalPrice= orderFound.total_price
        }
      })
      if (req.body.items) {
        const ItemIDs = req.body.items.map((item) => {
          return item.item;
        });
        const ItemResponse = await ItemModel.find(
          { _id: { $in: ItemIDs } },
          "_id name price"
        ).then((itemsDoc) => {
          return itemsDoc;
        });
        req.body.items.map((reqItems) => {
          ItemResponse.map((mongoItems) => {
            if (reqItems.item == mongoItems._id) {
              OrderQty += reqItems.qty;
              OrderTotalPrice += mongoItems.price * reqItems.qty;
              OrderDetailObjects = [
                ...OrderDetailObjects,
                {
                  restaurant: req.body.restaurant,
                  items: mongoItems._id,
                  qty: reqItems.qty,
                  unit_price: mongoItems.price,
                },
              ];
            }
          });
        });
      }
      if (req.body.modifiers) {
        const ModifierIDs = req.body.modifiers.map((modifier) => {
          return modifier.id;
        });
        console.log("modifier ids", ModifierIDs);
        const ModifierResponse = await ModifierModel.find(
          { _id: { $in: ModifierIDs } },
          "_id name price"
        ).then((modifiersDoc) => {
          return modifiersDoc;
        });

        console.log("ModifierResponse", ModifierResponse);
        req.body.modifiers.map((reqModifiers) => {
          ModifierResponse.map((mongoModifier) => {
            if (reqModifiers.id == mongoModifier._id) {
              OrderQty += reqModifiers.qty;
              OrderTotalPrice += mongoModifier.price * reqModifiers.qty;
              OrderDetailObjects = [
                ...OrderDetailObjects,
                {
                  restaurant: req.body.restaurant,
                  modifiers: mongoModifier._id,
                  qty: reqModifiers.qty,
                  unit_price: mongoModifier.price,
                },
              ];
            }
          });
        });
      }
      var orderObject = {
        total_qty: OrderQty,
        total_price: OrderTotalPrice,
      };
      console.log("order to update",orderObject)
      console.log("orderdetail to update",OrderDetailObjects)
      if (!errors.isEmpty()) {
        return apiResponse.validationErrorWithData(
          res,
          "Validation Error.",
          errors.array()
        );
      } else {
        if (!mongoose.Types.ObjectId.isValid(req.params.id)) {
          return apiResponse.validationErrorWithData(
            res,
            "Invalid Error.",
            "Invalid order ID"
          );
        } else {
          OrderModel.findById(req.params.id, function (err, foundOrder) {
            if (foundOrder === null) {
              return apiResponse.notFoundResponse(
                res,
                "Order not exists with this id"
              );
            } else {
              //update restaurant.
              console.log("working before update orderdetails");
              OrderDetailObjects.map((OrderDetailObject) => {
                OrderDetailObject["order"] = req.params.id;
              });
              OrderDetailModel.create(
                OrderDetailObjects,
                (err, OrderDeatailRes) => {
                  if (err) return apiResponse.ErrorResponse(res, err);
                  OrderModel.findByIdAndUpdate(
                    req.params.id,
                    orderObject,
                    {},
                    function (err) {
                      if (err) {
                        return apiResponse.ErrorResponse(res, err);
                      } else {
                        req.body.items.map((itemID)=>{
                          ItemModel.findByIdAndUpdate(itemID.item, { $inc: { sell_count: +itemID.qty } }, {},function (err) {
                          })
                        })
                        return apiResponse.successResponseWithData(
                          res,
                          "Order update success",
                          []
                        );
                      }
                    }
                  );
                }
              );
            }
          });
        }
      }
    } catch (err) {
      //throw error in json response with status 500.
      return apiResponse.ErrorResponse(res, err);
    }
  },
];

exports.orderStatusUpdate = [
  auth,
  accessValidation,
  body("type", "Status Type must not be empty.").isLength({ min: 1 }).trim(),
  body("restaurant", "Restaurant must not be empty.").isLength({ min: 1 }).trim(),
  sanitizeBody("type").escape(),
  sanitizeBody("restaurant").escape(),
  (req, res) => {
    try {
      console.log("body",req.body)
      console.log("params",req.params)
      const errors = validationResult(req);
      var statusUpdate={}
      if(req.body.type === "start"){
        statusUpdate = {
          is_pending: false,
          in_progress: true,
        };
      }
      else if(req.body.type === "finish"){
        statusUpdate = {
          is_pending: false,
          in_progress: false,
          is_ready: true,
          is_active:false
        };
      }else {
        if(req.body.type=== "cancel"){
          statusUpdate = {
            is_pending: false,
            in_progress: false,
            is_ready: false,
            is_cancel:true,
            is_active:false
          };
        }
      }
      if (!errors.isEmpty()) {
        return apiResponse.validationErrorWithData(
          res,
          "Validation Error.",
          errors.array()
        );
      } else {
        if (!mongoose.Types.ObjectId.isValid(req.params.id)) {
          return apiResponse.validationErrorWithData(
            res,
            "Invalid Error.",
            "Invalid order ID"
          );
        } else {
          console.log(req.params.id)
          OrderModel.findById(req.params.id, function (err, foundOrder) {
            if (foundOrder === null) {
              return apiResponse.notFoundResponse(
                res,
                "Order not exists with this id"
              );
            } else {
              //update Order.
              OrderModel.findByIdAndUpdate(
                req.params.id,
                statusUpdate,
                {},
                function (err) {
                  if (err) {
                    return apiResponse.ErrorResponse(res, err);
                  } else {
                    OrderModel.find({ restaurant: req.body.restaurant,is_active:true }).then((orders) => {
                      if (orders.length > 0) {
                        orderIDs = orders.map((order) => {
                          return order._id;
                        });
                        OrderDetailModel.find({ order: { $in: orderIDs } })
                          .populate("order")
                          .populate("items")
                          .then((orderDetails) => {
                            if (orderDetails.length > 0) {
                              return apiResponse.successResponseWithData(
                                res,
                                "Order status updated",
                                { orders, orderDetails }
                              );
                            } else {
                              return apiResponse.successResponseWithData(
                                res,
                                "Order status updated",
                                []
                              );
                            }
                          });
                      } else {
                        return apiResponse.successResponseWithData(res, "Order status updated", []);
                      }
                    });
                  }
                }
              );
            }
          });
        }
      }
    } catch (err) {
      //throw error in json response with status 500.
      return apiResponse.ErrorResponse(res, err);
    }
  },
];

exports.orderDetailStatusUpdate = [
  auth,
  accessValidation,
  body("restaurant", "Restaurant must not be empty.").isLength({ min: 1 }).trim(),
  sanitizeBody("restaurant").escape(),
  (req, res) => {
    try {
      const errors = validationResult(req);
      if (!errors.isEmpty()) {
        return apiResponse.validationErrorWithData(
          res,
          "Validation Error.",
          errors.array()
        );
      } else {
        if (!mongoose.Types.ObjectId.isValid(req.params.id)) {
          return apiResponse.validationErrorWithData(
            res,
            "Invalid Error.",
            "Invalid order Item ID"
          );
        } else {
          OrderDetailModel.findById(req.params.id)
          .populate('order')
          .then( (foundOrder,err )=> {
            if(err) console.log("its an error",err)
            if (!foundOrder) {
              return apiResponse.notFoundResponse(
                res,
                "Order not exists with this id"
              );
            } else {
              console.log("order found ",foundOrder)
              if(foundOrder.order.payment_status){
                return apiResponse.cascadeErrorWithData(res, "Invalid Attempt. Bill already paid",  []);
              }else{
                OrderDetailModel.findByIdAndUpdate(
                  req.params.id,
                  {is_canceled:true},
                  {},
                  function (err) {
                    if (err) {
                      return apiResponse.ErrorResponse(res, err);
                    } else {
                      OrderDetailModel.findById(req.params.id)
                      .populate('order')
                      .then((updatedOrderDetail)=>{
                        if(updatedOrderDetail.is_canceled){
                          OrderModel.findByIdAndUpdate(
                            updatedOrderDetail.order,
                            { $inc: 
                              { 
                                total_price: -(updatedOrderDetail.unit_price* updatedOrderDetail.qty),
                                total_qty: -(updatedOrderDetail.qty)
                              } 
                            },
                            {},
                            function (err) {
                              OrderModel.find({ restaurant: req.body.restaurant ,is_active:true}).then((orders) => {
                                if (orders.length > 0) {
                                  orderIDs = orders.map((order) => {
                                    return order._id;
                                  });
                                  OrderDetailModel.find({ order: { $in: orderIDs } })
                                    .populate("order")
                                    .populate("items")
                                    .then((orderDetails) => {
                                      if (orderDetails.length > 0) {
                                        return apiResponse.successResponseWithData(
                                          res,
                                          "Order status updated",
                                          { orders, orderDetails }
                                        );
                                      } else {
                                        return apiResponse.successResponseWithData(
                                          res,
                                          "Order status updated",
                                          []
                                        );
                                      }
                                    });
                                } else {
                                  return apiResponse.successResponseWithData(res, "Order status updated", []);
                                }
                              });
                            }
                          )
                        }else{
                          return apiResponse.cascadeErrorWithData(res, "Try again, Error occur while canceling item.", " ");  
                        }
                      })
                    }
                  }
                );
              }
              //update Order.
            }
          });
        }
      }
    } catch (err) {
      //throw error in json response with status 500.
      return apiResponse.ErrorResponse(res, err);
    }
  },
];

/**
 * Order Delete.
 *
 * @param {string}  
 *
 * @returns {Object}
 */
exports.orderDelete = [
  auth,
  accessValidation,
  function (req, res) {
    console.log(req.params);
    if (!mongoose.Types.ObjectId.isValid(req.params.id)) {
      return apiResponse.validationErrorWithData(
        res,
        "Invalid Error.",
        "Invalid ID"
      );
    }
    try {
      OrderModel.findById(req.params.id, function (err, foundOrder) {
        if (foundOrder === null) {
          return apiResponse.notFoundResponse(
            res,
            "Order not exists with this id "
          );
        } else {
          OrderDetailModel.deleteMany(
            { order: req.params.id },
            function (err, orderDetailDelete) {
              if (err) {
                return apiResponse.ErrorResponse(res, err);
              } else {
                OrderModel.findByIdAndRemove(req.params.id, function (err) {
                  if (err) {
                    return apiResponse.ErrorResponse(res, err);
                  } else {
                    return apiResponse.successResponseWithData(
                      res,
                      "Order delete success",
                      []
                    );
                  }
                });
              }
            }
          );
        }
      });
    } catch (err) {
      //throw error in json response with status 500.
      return apiResponse.ErrorResponse(res, err);
    }
  },
];

exports.softDeleteOrder = [
  auth,
  accessValidation,
  body("active")
    .isLength({ min: 1 })
    .withMessage("Active Status must be specified."),
  // Sanitize fields.
  sanitizeBody("active").escape(),
  // Process request after validation and sanitization.
  (req, res) => {
    if (!mongoose.Types.ObjectId.isValid(req.params.id)) {
      console.log("id error");
      return apiResponse.validationErrorWithData(
        res,
        "Invalid Error.",
        "Invalid ID"
      );
    }
    try {
      console.log(req.body);
      console.log(req.params);
      // Extract the validation errors from a request.
      const errors = validationResult(req);
      if (!errors.isEmpty()) {
        console.log(errors);
        // Display sanitized values/errors messages.
        return apiResponse.validationErrorWithData(
          res,
          "Validation Error.",
          errors.array()
        );
      } else {
        // Create User object with escaped and trimmed data
        var order = new OrderModel({
          active: req.body.active,
          _id: req.params.id,
        });
        if (!errors.isEmpty()) {
          console.log("error");
          return apiResponse.validationErrorWithData(
            res,
            "Validation Error.",
            errors.array()
          );
        } else {
          console.log("working 2");
          OrderModel.findById(req.params.id, function (err, foundOrder) {
            if (foundOrder === null) {
              console.log("order find error");
              return apiResponse.notFoundResponse(
                res,
                "Order not exists with this id"
              );
            } else {
              console.log("working 3");
              OrderModel.findByIdAndUpdate(
                req.params.id,
                order,
                {},
                function (err) {
                  if (err) return apiResponse.ErrorResponse(res, err);
                  return apiResponse.successResponseWithData(
                    res,
                    "Order delete success",
                    []
                  );
                }
              );
            }
          });
        }
      }
    } catch (err) {
      //throw error in json response with status 500.
      return apiResponse.ErrorResponse(res, err);
    }
  },
];


exports.orderPaymentStatusUpdate = [
  auth,
  accessValidation,
  body("restaurant", "Restaurant must not be empty.").isLength({ min: 1 }).trim(),
  sanitizeBody("restaurant").escape(),
  (req, res) => {
    try{
      console.log("body",req.body)
      console.log("params",req.params)
      const errors = validationResult(req);
      if (!errors.isEmpty()) {
        return apiResponse.validationErrorWithData(
          res,
          "Validation Error.",
          errors.array()
        );
      } else {
        console.log(req.body._id)
        OrderModel.find({_id:{$in:req.body._id}}, function (err, foundOrder) {
          if (foundOrder.length<0) {
            return apiResponse.notFoundResponse(
              res,
              "Orders not exist with these ids"
            );
          } else {
            //update Order.
            console.log("reqbody meh ids")
            OrderModel.updateMany(
              { _id: {$in:req.body._id} },
              { $set:{payment_status:true}},
              {},
              function (err) {
                if (err) {
                  return apiResponse.ErrorResponse(res, err);
                } else {
                  OrderModel.find({ restaurant: req.body.restaurant,is_active:true }).then((orders) => {
                    if (orders.length > 0) {
                      orderIDs = orders.map((order) => {
                        return order._id;
                      });
                      OrderDetailModel.find({ order: { $in: orderIDs } })
                        .populate("order")
                        .populate("items")
                        .then((orderDetails) => {
                          if (orderDetails.length > 0) {
                            return apiResponse.successResponseWithData(
                              res,
                              "Order status updated",
                              { orders, orderDetails }
                            );
                          } else {
                            return apiResponse.successResponseWithData(
                              res,
                              "Order status updated",
                              []
                            );
                          }
                        });
                    } else {
                      return apiResponse.successResponseWithData(res, "Order status updated", []);
                    }
                  });
                }
              }
            );
          }
        });
      }
    } catch (err) {
      //throw error in json response with status 500.
      return apiResponse.ErrorResponse(res, err);
    }
  },
];


// exports.orderPaymentStatusUpdate = [
//   auth,
//   accessValidation,
//   body("restaurant", "Restaurant must not be empty.").isLength({ min: 1 }).trim(),
//   sanitizeBody("restaurant").escape(),
//   (req, res) => {
//     try{
//       console.log("body",req.body)
//       console.log("params",req.params)
//       const errors = validationResult(req);
//       if (!errors.isEmpty()) {
//         return apiResponse.validationErrorWithData(
//           res,
//           "Validation Error.",
//           errors.array()
//         );
//       } else {
//         if (!mongoose.Types.ObjectId.isValid(req.params.id)) {
//           return apiResponse.validationErrorWithData(
//             res,
//             "Invalid Error.",
//             "Invalid order ID"
//           );
//         } else {
//           console.log(req.params.id)
//           OrderModel.findById(req.params.id, function (err, foundOrder) {
//             if (foundOrder === null) {
//               return apiResponse.notFoundResponse(
//                 res,
//                 "Order not exists with this id"
//               );
//             } else {
//               //update Order.
//               OrderModel.findByIdAndUpdate(
//                 req.params.id,
//                 {payment_status:true},
//                 {},
//                 function (err) {
//                   if (err) {
//                     return apiResponse.ErrorResponse(res, err);
//                   } else {
//                     OrderModel.find({ restaurant: req.body.restaurant,is_active:true }).then((orders) => {
//                       if (orders.length > 0) {
//                         orderIDs = orders.map((order) => {
//                           return order._id;
//                         });
//                         OrderDetailModel.find({ order: { $in: orderIDs } })
//                           .populate("order")
//                           .populate("items")
//                           .then((orderDetails) => {
//                             if (orderDetails.length > 0) {
//                               return apiResponse.successResponseWithData(
//                                 res,
//                                 "Order status updated",
//                                 { orders, orderDetails }
//                               );
//                             } else {
//                               return apiResponse.successResponseWithData(
//                                 res,
//                                 "Order status updated",
//                                 []
//                               );
//                             }
//                           });
//                       } else {
//                         return apiResponse.successResponseWithData(res, "Order status updated", []);
//                       }
//                     });
//                   }
//                 }
//               );
//             }
//           });
//         }
//       }
//     } catch (err) {
//       //throw error in json response with status 500.
//       return apiResponse.ErrorResponse(res, err);
//     }
//   },
// ];



exports.specificDateOrderList = [
  auth,
  accessValidation,
  function (req, res) {
    if (!mongoose.Types.ObjectId.isValid(req.params.id)) {
      return apiResponse.validationErrorWithData(
        res,
        "Invalid Restaurant Id",
        ""
      );
    }
    try {
      // let startDate=new Date()
      // let endDate = new Date()
      // console.log("start",startDate,"end",endDate)
      // console.log("start hours",startDate.getHours(),"end",endDate.getHours())
      let start = new Date(new Date(req.body.startDate).setHours(00, 00, 00))
      let end = new Date(new Date(req.body.endDate).setHours(23, 59, 59))
      // console.log("start",start,"end",end)
      // console.log("start hours",start.getHours(),"end",end.getHours())
      OrderModel.find({ 
        restaurant: req.params.id, 
        createdAt:{
          "$gte": start,
          "$lt": end
        }
      }).then((orders) => {
        if (orders.length > 0) {
          orderIDs = orders.map((order) => {
            return order._id;
          });
          OrderDetailModel.find({ order: { $in: orderIDs } })
            .populate("order")
            .populate("items")
            .then((orderDetails) => {
              if (orderDetails.length > 0) {
                return apiResponse.successResponseWithData(
                  res,
                  "List fetched success",
                  { orders, orderDetails }
                );
              } else {
                return apiResponse.successResponseWithData(
                  res,
                  "No data found",
                  []
                );
              }
            });
        } else {
          return apiResponse.successResponseWithData(res, "No data found", []);
        }
      });
    } catch (err) {
      return apiResponse.ErrorResponse(res, err);
    }
  },
];