const ModifierModel = require("../models/ModifierModel"); 
const Restaurant = require("../models/RestaurantModel");
const LanguageModel = require("../models/LanguageModel");
const { body,validationResult } = require("express-validator");
const { sanitizeBody } = require("express-validator");
const apiResponse = require("../helpers/apiResponse");
const auth = require("../middlewares/jwt");
const accessValidation = require("../middlewares/userValidation");
var mongoose = require("mongoose");
const ModifierGroupModel = require("../models/ModifierGroupModel");
mongoose.set("useFindAndModify", false);
const { uid } = require('uid');

/**
 * Category List. for specific restaurant
 * 
 * @returns {Object}
 */


exports.specificModifierList = [
	auth,
	accessValidation,
	function (req, res) {
		try {
			if(!mongoose.Types.ObjectId.isValid(req.params.id)){
				return apiResponse.validationErrorWithData(res, "Invalid Error.", "Invalid Restaurant ID");
			}else{
				ModifierModel
				.find({restaurant:req.params.id},"_id name urdu_name description price restaurant")
				.populate('restaurant','_id name')
				.then((modifiers)=>{
				if(modifiers.length > 0){
					return apiResponse.successResponseWithData(res, "List fetched success", modifiers);
				}else{
					return apiResponse.successResponseWithData(res, "No data found", []);
				}
			});
			}
		} catch (err) { 
			return apiResponse.ErrorResponse(res, err);
		}
	}
];

exports.modifierList = [
	auth,
	accessValidation,
	function (req, res) {
		try {
			console.log(req.user._id)
			if (req.user.user_type ==="owner"){
				Restaurant
				.find({owner: req.user._id},'_id owner')
				.then((restaurantList)=>{
					if(restaurantList === null){
						return apiResponse.notFoundResponse(res,"Restaurant not exists with this id");
					}else{
						restaurantIDs = restaurantList.map((singleRestaurant)=>{
							return singleRestaurant._id
						})
						ModifierModel
						.find({restaurant: {$in:restaurantIDs}}, "_id name urdu_name description price restaurant")
						.populate([{
							path: 'restaurant',
							select: '_id name'
						}])
						.then((modifiers)=>{
							if(modifiers.length > 0){
								return apiResponse.successResponseWithData(res, "List fetched success", modifiers);
							}else{
								return apiResponse.successResponseWithData(res, "No data found", []);
							}
						});
					}
				})
			}else{
				ModifierModel
				.find({},"_id name urdu_name description price restaurant")
				.populate('restaurant','name')
				.then((modifiers)=>{
					if(modifiers.length > 0){
						return apiResponse.successResponseWithData(res, "List fetched success", modifiers);
					}else{
						return apiResponse.successResponseWithData(res, "No data found", []);
					}
				});
			}        
		} catch (err) { 
			return apiResponse.ErrorResponse(res, err);
		}
	}
];

/**
 * Category store.
 * 
 * @param {string}      title 
 * @param {string}      description
 * @param {string}      isbn
 * 
 * @returns {Object}
 */
exports.modifierStore = [
	auth,
	accessValidation,
	body("name", "Name must not be empty.").isLength({ min: 1 }).trim(),
	body("price", "Price must not be empty.").isLength({ min: 1 }).trim(),
	body("restaurant", "Restaurant must not be Select.").isLength({ min: 1 }).trim(),
	sanitizeBody("*").escape(),
	(req, res) => {
		try {
			const errors = validationResult(req);
			var TransID = req.body.name.trim()
			TransID = `${TransID.replace(/ /g,'_')}_${uid(10)}`
			var modifier = new ModifierModel(
				{   
					name: req.body.name,
					urdu_name: req.body.urdu_name,
					description: req.body.description,
					price: req.body.price,
					restaurant: req.body.restaurant,
					trans_id: TransID
                });
			if (!errors.isEmpty()) {
				return apiResponse.validationErrorWithData(res, "Validation Error.", errors.array());
			}
			else {             
                //Save Modifier.
				modifier.save(function (err,modifierDoc) {
					if (err)  return apiResponse.ErrorResponse(res, err); 
					console.log("controller end",modifierDoc)
					var lang = new LanguageModel({   
						restaurant: modifierDoc.restaurant,
						trans_id: modifierDoc.trans_id,
						en_name: modifierDoc.name,
						urdu_name: modifierDoc.urdu_name ? modifierDoc.urdu_name : modifierDoc.name 
					});
					lang.save(function (err,langaugeDoc) {
						if (err) return apiResponse.ErrorResponse(res, err); 
						if (req.user.user_type ==="owner"){
							Restaurant
							.find({owner: req.user._id},'_id owner')
							.then((restaurantList)=>{
								if(restaurantList === null){
									return apiResponse.notFoundResponse(res,"Restaurant not exists with this id");
								}else{
									restaurantIDs = restaurantList.map((singleRestaurant)=>{
										return singleRestaurant._id
									})
									ModifierModel
									.find({restaurant: {$in:restaurantIDs}}, "_id name urdu_name description price restaurant")
									.populate([{
										path: 'restaurant',
										select: '_id name'
									}])
									.then((modifiers)=>{
										if(modifiers.length > 0){
											return apiResponse.successResponseWithData(res, "Modifier add success", modifiers);
										}else{
											return apiResponse.successResponseWithData(res, "Modifier add found", []);
										}
									});
								}
							})
						}else{
							ModifierModel
							.find({},"_id name urdu_name description price restaurant")
							.populate('restaurant','name')
							.then((modifiers)=>{
								if(modifiers.length > 0){
									return apiResponse.successResponseWithData(res, "Modifier add success", modifiers);
								}else{
									return apiResponse.successResponseWithData(res, "Modifier add found", []);
								}
							});
						}
					});
				});
			}
		} catch (err) {
			//throw error in json response with status 500. 
			return apiResponse.ErrorResponse(res, err);
		}
	}
];

/**
 * Category update.
 * 
 * @param {string}      title 
 * @param {string}      description
 * @param {string}      isbn
 * 
 * @returns {Object}
 */
exports.modifierUpdate = [
	auth,
	accessValidation,
	body("name", "Name must not be empty.").isLength({ min: 1 }).trim(),
	body("price", "Price must not be empty.").isLength({ min: 1 }).trim(),
	body("restaurant", "Restaurant must not be Select.").isLength({ min: 1 }).trim(),
	sanitizeBody("*").escape(),
	(req, res) => {
		try {
			const errors = validationResult(req);
			console.log(req.body)
			var modifier = new ModifierModel(
                {   
					name: req.body.name,
					urdu_name: req.body.urdu_name,
					description: req.body.description,
					restaurant: req.body.restaurant,
					price: req.body.price,
					_id:req.params.id
				});
			if (!errors.isEmpty()) {
				return apiResponse.validationErrorWithData(res, "Validation Error.", errors.array());
			}
			else {
				if(!mongoose.Types.ObjectId.isValid(req.params.id)){
					return apiResponse.validationErrorWithData(res, "Invalid Error.", "Invalid modifier ID");
				}else{
					ModifierModel.findById(req.params.id, function (err, foundModifier) {
						if(foundModifier === null){
							return apiResponse.notFoundResponse(res,"Modifier not exists with this id");
						}else{
							//update restaurant.
							ModifierModel.findByIdAndUpdate(req.params.id, modifier, {},function (err,afterUpdateMod) {
								if (err) { 
									return apiResponse.ErrorResponse(res, err); 
								}else{
									LanguageModel.findOneAndUpdate(
										{
											restaurant:afterUpdateMod.restaurant,
											trans_id:afterUpdateMod.trans_id
										},{ 
											en_name:req.body.name,
											urdu_name:req.body.urdu_name ? req.body.urdu_name : req.body.name
										},{},(err,afterupdate)=>{
										if (err) {
											return apiResponse.ErrorResponse(res, err); 
										}else{
											if (req.user.user_type ==="owner"){
												Restaurant
												.find({owner: req.user._id},'_id owner')
												.then((restaurantList)=>{
													if(restaurantList === null){
														return apiResponse.notFoundResponse(res,"Restaurant not exists with this id");
													}else{
														restaurantIDs = restaurantList.map((singleRestaurant)=>{
															return singleRestaurant._id
														})
														ModifierModel
														.find({restaurant: {$in:restaurantIDs}}, "_id name urdu_name description price restaurant")
														.populate([{
															path: 'restaurant',
															select: '_id name'
														}])
														.then((modifiers)=>{
															if(modifiers.length > 0){
																return apiResponse.successResponseWithData(res, "Modifier update success", modifiers);
															}else{
																return apiResponse.successResponseWithData(res, "Modifier update found", []);
															}
														});
													}
												})
											}else{
												ModifierModel
												.find({},"_id name urdu_name description price restaurant")
												.populate('restaurant','name')
												.then((modifiers)=>{
													if(modifiers.length > 0){
														return apiResponse.successResponseWithData(res, "Modifier update success", modifiers);
													}else{
														return apiResponse.successResponseWithData(res, "Modifier update found", []);
													}
												});
											}	
										}
									})		
								}
							});
						}
					});
				}
			}
		} catch (err) {
			//throw error in json response with status 500. 
			return apiResponse.ErrorResponse(res, err);
		}
	}
];

/**
 * Category Delete.
 * 
 * @param {string}      id
 * 
 * @returns {Object}
 */
exports.modifierDelete = [
	auth,
	accessValidation,
	function (req, res) {
		console.log(req.params)
		if(!mongoose.Types.ObjectId.isValid(req.params.id)){
			return apiResponse.validationErrorWithData(res, "Invalid Error.", "Invalid ID");
		}
		try {
			ModifierModel.findById(req.params.id, function (err, foundModifier) {
				if(foundModifier === null){
					return apiResponse.notFoundResponse(res,"Modifier not exists with this id ");
				}else{
					ModifierGroupModel.find({ modifier: { "$in" : [req.params.id]} }, function (err, modifierCheck) {
						if(modifierCheck.length>0){
							console.log("query run")
							return apiResponse.cascadeErrorWithData(res, "Invalid Attempt. Delete modifier associate modifier group first", " ");
						}else{
							ModifierModel.findByIdAndRemove(req.params.id,function (err) {
								if (err) { 
									return apiResponse.ErrorResponse(res, err); 
								}else{
									if (req.user.user_type ==="owner"){
										Restaurant
										.find({owner: req.user._id},'_id owner')
										.then((restaurantList)=>{
											if(restaurantList === null){
												return apiResponse.notFoundResponse(res,"Restaurant not exists with this id");
											}else{
												restaurantIDs = restaurantList.map((singleRestaurant)=>{
													return singleRestaurant._id
												})
												ModifierModel
												.find({restaurant: {$in:restaurantIDs}}, "_id name urdu_name description price restaurant")
												.populate([{
													path: 'restaurant',
													select: '_id name'
												}])
												.then((modifiers)=>{
													if(modifiers.length > 0){
														return apiResponse.successResponseWithData(res, "Modifier delete success", modifiers);
													}else{
														return apiResponse.successResponseWithData(res, "Modifier delete found", []);
													}
												});
											}
										})
									}else{
										ModifierModel
										.find({},"_id name urdu_name description price restaurant")
										.populate('restaurant','name')
										.then((modifiers)=>{
											if(modifiers.length > 0){
												return apiResponse.successResponseWithData(res, "Modifier delete success", modifiers);
											}else{
												return apiResponse.successResponseWithData(res, "Modifier delete found", []);
											}
										});
									}
								}
							});
						}
					})
				}
			});
		} catch (err) {
			//throw error in json response with status 500. 
			return apiResponse.ErrorResponse(res, err);
		}
	}
];