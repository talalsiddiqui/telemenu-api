const UserModel = require("../models/UserModel");
const { body, validationResult } = require("express-validator");
const { sanitizeBody } = require("express-validator");
const apiResponse = require("../helpers/apiResponse");
const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");
const auth = require("../middlewares/jwt");
const teleAccess = require("../middlewares/teleAccess");

var mongoose = require("mongoose");
mongoose.set("useFindAndModify", false);


/**
 * User registration.
 *
 * @param {string}      firstName
 * @param {string}      lastName
 * @param {string}      email
 * @param {string}      password
 *
 * @returns {Object}
 */
exports.register = [
  // Validate fields.
  auth,
  teleAccess,
  body("firstName")
    .isLength({ min: 1 })
    .trim()
    .withMessage("First name must be specified.")
    .isAlphanumeric()
    .withMessage("First name has non-alphanumeric characters."),
  body("lastName")
    .isLength({ min: 1 })
    .trim()
    .withMessage("Last name must be specified.")
    .isAlphanumeric()
    .withMessage("Last name has non-alphanumeric characters."),
  body("user_type")
    .isLength({ min: 1 })
    .trim()
    .withMessage("User Type must be specified.")
    .isAlphanumeric()
    .withMessage("User Type has non-alphanumeric characters."),
  body("email")
    .isLength({ min: 1 })
    .trim()
    .withMessage("Email must be specified.")
    .isEmail()
    .withMessage("Email must be a valid email address.")
    .custom((value) => {
      return UserModel.findOne({ email: value }).then((user) => {
        if (user) {
          return Promise.reject("E-mail already in use");
        }
      });
    }),
  body("password")
    .isLength({ min: 6 })
    .trim()
    .withMessage("Password must be 6 characters or greater."),
  sanitizeBody("firstName").escape(),
  sanitizeBody("lastName").escape(),
  sanitizeBody("user_type").escape(),
  sanitizeBody("email").escape(),
  sanitizeBody("password").escape(),
  // Process request after validation and sanitization.
  (req, res) => {
    try {
      // Extract the validation errors from a request.
      const errors = validationResult(req);
      if (!errors.isEmpty()) {
        // Display sanitized values/errors messages.
        return apiResponse.validationErrorWithData(
          res,
          "Validation Error.",
          errors.array()
        );
      } else {
        //hash input password
        bcrypt.hash(req.body.password, 10, function (err, hash) {
          // Create User object with escaped and trimmed data
          var user = new UserModel({
            firstName: req.body.firstName,
            lastName: req.body.lastName,
            email: req.body.email,
            user_type: req.body.user_type,
            password: hash,
          });
          user.save(function (err) {
            if (err) {
              return apiResponse.ErrorResponse(res, err);
            }
            UserModel.find({},"_id firstName lastName email user_type active")
            .then((users) => {
              if (users.length > 0) {
                return apiResponse.successResponseWithData(res,"Registration Success",users);
              } else {
                return apiResponse.successResponseWithData(res,"Registration Success.",[]);
              }
            });
          });
        });
      }
    } catch (err) {
      //throw error in json response with status 500.
      return apiResponse.ErrorResponse(res, err);
    }
  },
];

/**
 * User login.
 *
 * @param {string}      email
 * @param {string}      password
 *
 * @returns {Object}
 */
exports.login = [
  body("email")
    .isLength({ min: 1 })
    .trim()
    .withMessage("Email must be specified.")
    .isEmail()
    .withMessage("Email must be a valid email address."),
  body("password")
    .isLength({ min: 1 })
    .trim()
    .withMessage("Password must be specified."),
  sanitizeBody("email").escape(),
  sanitizeBody("password").escape(),
  
  (req, res) => {
    try {
      console.log("login working 1")
      const errors = validationResult(req);
      if (!errors.isEmpty()) {
        console.log("login working error 1")
        return apiResponse.validationErrorWithData(
          res,
          "Validation Error.",
          errors.array()
        );
      } else {
        console.log("login working 2")
        console.log(req.body.email)
        UserModel.findOne({ email: req.body.email }).then((userResult) => {
          if (userResult) {
            console.log("login working 3")
            if(userResult.active===false){
              console.log("login working error 2")
              return apiResponse.unauthorizedResponse(
                res,
                "Unauthorized to login, contact Telemenu."
              );
            }else{
              bcrypt.compare(req.body.password, userResult.password, function (err,same) {
                if (same) {
                  let userData = {
                    _id: userResult._id,
                    firstName: userResult.firstName,
                    lastName: userResult.lastName,
                    user_type: userResult.user_type,
                    active:userResult.active,
                    email: userResult.email,
                  };
                  //Prepare JWT token for authentication
                  const jwtPayload = userData;
                  const jwtData = {
                    expiresIn: process.env.JWT_TIMEOUT_DURATION || "2 hours",
                  };
                  const secret = process.env.JWT_SECRET || "3343abcd890lsncjdbhehbcoerlvehrbov";
                  //Generated JWT token with Payload and secret.
                  userData.token = jwt.sign(jwtPayload, secret, jwtData);
                  return apiResponse.successResponseWithData(
                    res,
                    "Login Success.",
                    userData
                  );
                } else {
                  return apiResponse.unauthorizedResponse(
                    res,
                    "Invalid credentials."
                  );
                }
              });
            }
          }
            //Compare given password with db's hash.
          else {
            console.log("login user not found")
            return apiResponse.unauthorizedResponse(
              res,
              "Email or Password wrong."
            );
          }
        });
      }
    } catch (err) {
      return apiResponse.ErrorResponse(res, err);
    }
  },
];

/**
 * Restaurant Owners List.
 *
 * @returns {Object}
 */
exports.restaurantOwersList = [
  auth,
  // teleAccess,
  function (req, res) {
    try {
      UserModel.find(
        { user_type: "owner" },
        "_id firstName lastName email user_type active"
      ).then((users) => {
        if (users.length > 0) {
          return apiResponse.successResponseWithData(
            res,
            "Owner list fetched",
            users
          );
        } else {
          return apiResponse.successResponseWithData(
            res,
            "No data found",
            []
          );
        }
      });
    } catch (err) {
      //throw error in json response with status 500.
      return apiResponse.ErrorResponse(res, err);
    }
  },
];


exports.getSingleUser = [
  auth,
  // accessValidation,
  function (req, res) {
    try {
      console.log("query ",req.query)
      console.log("params",req.params)
      UserModel.findById(req.params.id,"_id firstName lastName email user_type active"
      ).then((users) => {
        console.log("response",users)
        if (users) {
          return apiResponse.successResponseWithData(
            res,
            "User fetched",
            users
          );
        } else {
          return apiResponse.successResponseWithData(
            res,
            "No data found",
            []
          );
        }
      });
    } catch (err) {
      //throw error in json response with status 500.
      return apiResponse.ErrorResponse(res, err);
    }
  },
];


/**
 * Users List.
 *
 * @returns {Object}
 */
exports.userList = [
  auth,
  function (req, res) {
    console.log("working list");
    try {
      UserModel.find({},"_id firstName lastName email user_type active")
      .then((users) => {
        if (users.length > 0) {
          return apiResponse.successResponseWithData(res,"List Fetched",users);
        } else {
          return apiResponse.successResponseWithData(res,"No Data Found",[]);
        }
      });
    } catch (err) {
      //throw error in json response with status 500.
      return apiResponse.ErrorResponse(res, err);
    }
  },
];

exports.updateUser = [
  auth,
	teleAccess,
  body("firstName")
    .isLength({ min: 1 })
    .trim()
    .withMessage("First name must be specified.")
    .isAlphanumeric()
    .withMessage("First name has non-alphanumeric characters."),
  body("lastName")
    .isLength({ min: 1 })
    .trim()
    .withMessage("Last name must be specified.")
    .isAlphanumeric()
    .withMessage("Last name has non-alphanumeric characters."),
  body("user_type")
    .isLength({ min: 1 })
    .trim()
    .withMessage("User Type must be specified.")
    .isAlphanumeric()
    .withMessage("User Type has non-alphanumeric characters."),
  body("email")
    .isLength({ min: 1 })
    .trim()
    .withMessage("Email must be specified.")
    .isEmail()
    .withMessage("Email must be a valid email address.")
    .custom((value) => {
      return UserModel.find({ email: value }).then((user) => {
        if (user) {
          console.log("email error")
          if(user.length>1){
            console.log("email exist error")
            return Promise.reject("E-mail already in use");
          }
        }
      });
    }),
  // Sanitize fields.
  sanitizeBody("firstName").escape(),
  sanitizeBody("lastName").escape(),
  sanitizeBody("user_type").escape(),
  sanitizeBody("email").escape(),
  // Process request after validation and sanitization.
  (req, res) => {
    if(!mongoose.Types.ObjectId.isValid(req.params.id)){
      console.log("id error")
      return apiResponse.validationErrorWithData(
        res,
        "Invalid Error.",
        "Invalid ID"
      );
    }
    try {
      console.log(req.body)
      console.log(req.params)
      // Extract the validation errors from a request.
      const errors = validationResult(req);
      if (!errors.isEmpty()) {
        console.log(errors)
        // Display sanitized values/errors messages.
        return apiResponse.validationErrorWithData(
          res,
          "Validation Error.",
          errors.array()
        );
      } else {
        //hash input password
        // Create User object with escaped and trimmed data
        var user = new UserModel({
          firstName: req.body.firstName,
          lastName: req.body.lastName,
          email: req.body.email,
          user_type: req.body.user_type,
          _id: req.params.id
        });
        if (!errors.isEmpty()) {
          console.log("error")
          return apiResponse.validationErrorWithData(res, "Validation Error.", errors.array());
        }else{
          console.log("working 2")
          UserModel.findById(req.params.id, function (err, foundUser) {
            if(foundUser === null){
              console.log("user find error")
              return apiResponse.notFoundResponse(res,"User not exists with this id");
            }else{
              console.log("working 3")
              UserModel.findByIdAndUpdate(req.params.id, user, {},function (err) {
                if (err) { 
                  return apiResponse.ErrorResponse(res, err); 
                }else{
                  UserModel.find({},"_id firstName lastName email password user_type active")
                  .then((users) => {
                    if (users.length > 0) {
                      return apiResponse.successResponseWithData(res,"User update Success",users);
                    } else {
                      return apiResponse.successResponseWithData(res,"User update Success", []);
                    }
                  });
                }
              });
            }
          })
        }
      }
    } catch (err) {
      //throw error in json response with status 500.
      return apiResponse.ErrorResponse(res, err);
    }
  },
];


/**
 * User Delete.
 *
 * @param {string}      id
 *
 * @returns {Object}
 */
exports.userDelete = [
  auth,
  teleAccess,
  function (req, res) {
    console.log(req.params);
    if (!mongoose.Types.ObjectId.isValid(req.params.id)) {
      return apiResponse.validationErrorWithData(
        res,
        "Invalid Error.",
        "Invalid ID"
      );
    }
    try {
      UserModel.findById(req.params.id, function (err, foundUser) {
        if (foundUser === null) {
          return apiResponse.notFoundResponse(
            res,
            "User not exists with this id"
          );
        } else {
          //delete Restaurant.
          UserModel.findByIdAndRemove(req.params.id, function (err) {
            if (err) {
              return apiResponse.ErrorResponse(res, err);
            } else {
              return apiResponse.successResponse(res, "User delete Success.");
            }
          });
        }
      });
    } catch (err) {
      //throw error in json response with status 500.
      return apiResponse.ErrorResponse(res, err);
    }
  },
];



exports.updateUserActiveness = [
  auth,
	teleAccess,
  body("active")
    .isLength({ min: 1 })
    .withMessage("Active Status must be specified."),
  // Sanitize fields.
  sanitizeBody("active").escape(),
  // Process request after validation and sanitization.
  (req, res) => {
    if(!mongoose.Types.ObjectId.isValid(req.params.id)){
      console.log("id error")
      return apiResponse.validationErrorWithData(
        res,
        "Invalid Error.",
        "Invalid ID"
      );
    }
    try {
      console.log(req.body)
      console.log(req.params)
      // Extract the validation errors from a request.
      const errors = validationResult(req);
      if (!errors.isEmpty()) {
        console.log(errors)
        // Display sanitized values/errors messages.
        return apiResponse.validationErrorWithData(
          res,
          "Validation Error.",
          errors.array()
        );
      } else {
        // Create User object with escaped and trimmed data
        var user = new UserModel({
          active:req.body.active,
          _id: req.params.id
        });
        if (!errors.isEmpty()) {
          console.log("error")
          return apiResponse.validationErrorWithData(res, "Validation Error.", errors.array());
        }else{
          console.log("working 2")
          UserModel.findById(req.params.id, function (err, foundUser) {
            if(foundUser === null){
              console.log("user find error")
              return apiResponse.notFoundResponse(res,"User not exists with this id");
            }else{
              console.log("working 3")
              UserModel.findByIdAndUpdate(req.params.id, user, {},function (err) {
                if (err) { 
                  return apiResponse.ErrorResponse(res, err); 
                }else{
                  UserModel.find({},"_id firstName lastName email user_type active")
                  .then((users) => {
                    if (users.length > 0) {
                      return apiResponse.successResponseWithData(res,"User Status update Success",users);
                    } else {
                      return apiResponse.successResponseWithData(res,"User Status update Success", []);
                    }
                  });
                }
              });
            }
          })
        }
      }
    } catch (err) {
      //throw error in json response with status 500.
      return apiResponse.ErrorResponse(res, err);
    }
  },
];
