const Restaurant = require("../models/RestaurantModel");
const CategoryModel = require("../models/CategoryModel");
const ItemModel = require("../models/ItemModel");
const MenuModel = require("../models/MenuModel");
const ModifierGroupModel = require("../models/ModifierGroupModel");
const ModifierModel = require("../models/ModifierModel");
const SectionModel = require("../models/SectionModel");
const teleAccess = require("../middlewares/teleAccess");
const { body,validationResult } = require("express-validator");
const { sanitizeBody } = require("express-validator");
const apiResponse = require("../helpers/apiResponse");
const auth = require("../middlewares/jwt");
const accessValidation = require("../middlewares/userValidation");
var mongoose = require("mongoose");
mongoose.set("useFindAndModify", false);

// Restaurant Schema

function RestaurantData(data) {
	this.id = data._id;
	this.name = data.name;
	this.address = data.address;
    this.contact_number = data.contact_number;
	this.country = data.country;
	this.restaurant_type = data.restaurant_type;
	this.currency_type = data.currency_type;
    this.payment_method = data.payment_method;
    this.createdAt = data.createdAt;
	this.owner = data.owner
	this.logo = data.logo;
	this.banner= data.banner
}

/**
 * Restaurant List.
 * 
 * @returns {Object}
 */
exports.restaurantList = [
	auth,
	accessValidation,
	function (req, res) {
		try {
			console.log(req.user)
			let findObj ={};
			if (req.user.user_type ==="owner"){
				findObj['owner'] = req.user._id;
			}
			Restaurant
			.find( findObj )
			.populate('owner','email')
			.then((restaurants)=>{
				if(restaurants.length > 0){
					return apiResponse.successResponseWithData(res, "Operation success", restaurants);
				}else{
					return apiResponse.successResponseWithData(res, "Operation success", []);
				}
			});
		}catch (err) { 
			return apiResponse.ErrorResponse(res, err);
		}
	}
];

/**
 * Restaurant Detail.
 * 
 * @param {string}      id
 * 
 * @returns {Object}
 */
exports.restaurantDetail = [
	auth,
	accessValidation,
	function (req, res) {
		if(!mongoose.Types.ObjectId.isValid(req.params.id)){
			return apiResponse.successResponseWithData(res, "Operation success", {});
		}
		try {
			Restaurant.findOne({_id: req.params.id,user: req.user._id},"_id title description isbn createdAt").then((restaurant)=>{                
				if(restaurant !== null){
					let restaurantData = new RestaurantData(restaurant);
					return apiResponse.successResponseWithData(res, "Operation success", restaurantData);
				}else{
					return apiResponse.successResponseWithData(res, "Operation success", {});
				}
			});
		} catch (err) {
			//throw error in json response with status 500. 
			return apiResponse.ErrorResponse(res, err);
		}
	}
];

/**
 * Restaurant store.
 * 
 * @param {string}      title 
 * @param {string}      description
 * @param {string}      isbn
 * 
 * @returns {Object}
 */
exports.restaurantStore = [
	auth,
	teleAccess,
	body("name", "Name must not be empty.").isLength({ min: 1 }).trim(),
	body("address", "Address must not be empty.").isLength({ min: 1 }).trim(),
	body("contact_number", "Contact Number must not be empty.").isLength({ min: 1 }).trim(),
	body("country", "Country must not be empty.").isLength({ min: 1 }).trim(),
	body("restaurant_type", "Restaurant Type must be Selected.").isLength({ min: 1 }).trim(),
	body("currency_type", "Currency Type must be Selected.").isLength({ min: 1 }).trim(),
	body("payment_method", "Payment Method must be Selected.").isLength({ min: 1 }).trim(),
	body("owner", "Owner must be Selected.").isLength({ min: 1 }).trim(),
	// body("isbn", "ISBN must not be empty").isLength({ min: 1 }).trim().custom((value,{req}) => {
	// 	return Book.findOne({isbn : value,user: req.user._id}).then(book => {
	// 		if (book) {
	// 			return Promise.reject("Book already exist with this ISBN no.");
	// 		}
	// 	});
	// }),
	sanitizeBody("name").escape(),
	sanitizeBody("address").escape(),
	sanitizeBody("contact_number").escape(),
	sanitizeBody("country").escape(),
	sanitizeBody("restaurant_type").escape(),
	sanitizeBody("currency_type").escape(),
	sanitizeBody("payment_method").escape(),
	sanitizeBody("owner").escape(),
	(req, res) => {
		try {
			console.log("add rest req",req.body)
			const errors = validationResult(req);
			var restaurant = new Restaurant(
				{   name: req.body.name,
					address: req.body.address,
					contact_number: req.body.contact_number,
                    country: req.body.country,
                    restaurant_type: req.body.restaurant_type,
					currency_type: req.body.currency_type,
					payment_method: req.body.payment_method,
					readonly: req.body.readonly,
					owner:req.body.owner,
					logo: req.body.logo,
                    banner: req.body.banner,
				});
				console.log(restaurant)
			if (!errors.isEmpty()) {
				return apiResponse.validationErrorWithData(res, "Validation Error.", errors.array());
			}
			else {             
				//Save Restaurant.
				restaurant.save(function (err,restaurantSavedDoc) {
					if (err)  return apiResponse.ErrorResponse(res, err); 
					Restaurant
					.find( {} )
					.populate('owner','email')
					.then((afterSavedRestaurantList)=>{
						if(afterSavedRestaurantList.length > 0){
							return apiResponse.successResponseWithData(res,"Restaurant add Success.", afterSavedRestaurantList);
						}else{
							return apiResponse.successResponseWithData(res, "Restaurant add Success.", []);
						}
					});
				});
			}
		}catch (err) {
			//throw error in json response with status 500. 
			return apiResponse.ErrorResponse(res, err);
		}
	}
];


/**
 * Restaurant update.
 * 
 * @param {string}      title 
 * @param {string}      description
 * @param {string}      isbn
 * 
 * @returns {Object}
 */
exports.restaurantUpdate = [
	auth,
	teleAccess,
	body("name", "Name must not be empty.").isLength({ min: 1 }).trim(),
	body("address", "Address must not be empty.").isLength({ min: 1 }).trim(),
	body("contact_number", "Contact Number must not be empty.").isLength({ min: 1 }).trim(),
	body("country", "Country must not be empty.").isLength({ min: 1 }).trim(),
	body("restaurant_type", "Restaurant Type must not be empty.").isLength({ min: 1 }).trim(),
	body("currency_type", "Currency Type must not be empty.").isLength({ min: 1 }).trim(),
	body("payment_method", "Payment Method must not be empty.").isLength({ min: 1 }).trim(),
	body("owner", "Owner must not be Selected.").isLength({ min: 1 }).trim(),
	// body("isbn", "ISBN must not be empty").isLength({ min: 1 }).trim().custom((value,{req}) => {
	// 	return Book.findOne({isbn : value,user: req.user._id, _id: { "$ne": req.params.id }}).then(book => {
	// 		if (book) {
	// 			return Promise.reject("Book already exist with this ISBN no.");
	// 		}
	// 	});
	// }),
	sanitizeBody("name").escape(),
	sanitizeBody("address").escape(),
	sanitizeBody("contact_number").escape(),
	sanitizeBody("country").escape(),
	sanitizeBody("restaurant_type").escape(),
	sanitizeBody("currency_type").escape(),
	sanitizeBody("payment_method").escape(),
	sanitizeBody("owner").escape(),
	(req, res) => {
		try {
			const errors = validationResult(req);
			console.log(req.body)
			var restaurant = new Restaurant(
                {   name: req.body.name,
					address: req.body.address,
					contact_number: req.body.contact_number,
                    country: req.body.country,
                    restaurant_type: req.body.restaurant_type,
					currency_type: req.body.currency_type,
                    payment_method: req.body.payment_method,
					owner:req.body.owner,
					readonly: req.body.readonly,
					logo: req.body.logo,
                    banner: req.body.banner,
					_id:req.params.id
				});
			if (!errors.isEmpty()) {
				return apiResponse.validationErrorWithData(res, "Validation Error.", errors.array());
			}
			else {
				if(!mongoose.Types.ObjectId.isValid(req.params.id)){
					return apiResponse.validationErrorWithData(res, "Invalid Error.", "Invalid ID");
				}else{
					Restaurant.findById(req.params.id, function (err, foundRestaurant) {
						if(foundRestaurant === null){
							return apiResponse.notFoundResponse(res,"Restaurant not exists with this id");
						}else{
							//update restaurant.
							Restaurant.findByIdAndUpdate(req.params.id, restaurant, {},function (err) {
								if (err) { 
									return apiResponse.ErrorResponse(res, err); 
								}else{
									Restaurant
									.find( {} )
									.populate('owner','email')
									.then((afterEditRestaurantList)=>{
										if(afterEditRestaurantList.length > 0){
											return apiResponse.successResponseWithData(res,"Restaurant Update Success.", afterEditRestaurantList);
										}else{
											return apiResponse.successResponseWithData(res, "Restaurant Update Success.", []);
										}
									});
								}
							});
						}
					});
				}
			}
		} catch (err) {
			//throw error in json response with status 500. 
			return apiResponse.ErrorResponse(res, err);
		}
	}
];

/**
 * Restaurant Delete.
 * 
 * @param {string}      id
 * 
 * @returns {Object}
 */
exports.restaurantDelete = [
	auth,
	teleAccess,
	function (req, res) {
		console.log(req.params)
		if(!mongoose.Types.ObjectId.isValid(req.params.id)){
			return apiResponse.validationErrorWithData(res, "Invalid Error.", "Invalid ID");
		}
		try {
			Restaurant.findById(req.params.id, function (err, foundRestaurant) {
				if(foundRestaurant === null){
					return apiResponse.notFoundResponse(res,"Restaurant not exists with this id");
				}else{
					CategoryModel.find({restaurant: req.params.id}, function (err, categoryCheck) {
						if(categoryCheck.length>0){
							console.log("category exist")
							return apiResponse.cascadeErrorWithData(res, "Invalid Attempt. Delete restaurant associate category first", " ");
						}else{
							console.log("category not exist")
							ItemModel.find({restaurant: req.params.id}, function (err, itemCheck) {
								if(itemCheck.length>0){
									return apiResponse.cascadeErrorWithData(res, "Invalid Attempt. Delete restaurant associate Items first", " ");
								}else{
									SectionModel.find({restaurant: req.params.id}, function (err, sectionCheck) {
										if(sectionCheck.length>0){
											return apiResponse.cascadeErrorWithData(res, "Invalid Attempt. Delete restaurant associate section first", " ");
										}else{
											MenuModel.find({restaurant: req.params.id}, function (err, menuCheck) {
												if(menuCheck.length>0){
													return apiResponse.cascadeErrorWithData(res, "Invalid Attempt. Delete restaurant associate menu first", " ");
												}else{
													ModifierModel.find({restaurant: req.params.id}, function (err, modifierCheck) {
														if(modifierCheck.length>0){
															return apiResponse.cascadeErrorWithData(res, "Invalid Attempt. Delete restaurant associate modifier first", " ");
														}else{
															ModifierGroupModel.find({restaurant: req.params.id}, function (err, modifiergroupCheck) {
																if(modifiergroupCheck.length>0){
																	return apiResponse.cascadeErrorWithData(res, "Invalid Attempt. Delete restaurant associate modifier group first", " ");
																}else{
																	Restaurant.findByIdAndRemove(req.params.id,function (err) {
																		if (err) { 
																			return apiResponse.ErrorResponse(res, err); 
																		}else{
																			Restaurant
																			.find( {} )
																			.populate('owner','email')
																			.then((afterDeleteRestaurantList)=>{
																				if(afterDeleteRestaurantList.length > 0){
																					return apiResponse.successResponseWithData(res,"Restaurant delete Success.", afterDeleteRestaurantList);
																				}else{
																					return apiResponse.successResponseWithData(res, "Restaurant delete Success.", []);
																				}
																			});
																		}
																	});
																}
															})
														}
													})
												}
											})		
										}
									})
								}
							})
						}
					})
					//delete Restaurant.
				}
			});
		}catch (err) {
			//throw error in json response with status 500. 
			return apiResponse.ErrorResponse(res, err);
		}
	}
];