const Restaurant = require("../models/RestaurantModel");
const UserModel = require("./../models/UserModel")
const { body,validationResult } = require("express-validator");
const { sanitizeBody } = require("express-validator");
const apiResponse = require("../helpers/apiResponse");
const auth = require("../middlewares/jwt");
const accessValidation = require("../middlewares/userValidation");
var mongoose = require("mongoose");
const ItemModel = require("../models/ItemModel");
const OrderModel = require("../models/OrderModel");
mongoose.set("useFindAndModify", false);


/**
 * Dashboard Detail.
 * 
 * @param {string}      id
 * 
 * @returns {Object}
 */
exports.dashboard = [
	auth,
	accessValidation,
	function (req, res) {
		if(!mongoose.Types.ObjectId.isValid(req.params.id)){
			return apiResponse.successResponseWithData(res, "Operation success", {});
		}
		try {
            if (req.user.user_type ==="owner"){
                let start = new Date(new Date()).setHours(00, 00, 00)
                let end = new Date(new Date()).setHours(23, 59, 59)
                OrderModel.find({ 
                    restaurant: req.params.id, 
                    payment_status: true,
                    createdAt:{
                    "$gte": start,
                    "$lt": end
                    }
                }).then((specificOrders) => {
                    // specificOrders.reduce(
                    //     (preOrder, nextOrder) => preOrder.total_price + nextOrder.total_price, 0
                    // )
                    OrderModel.find({ 
                        restaurant: req.params.id, 
                        payment_status: true,
                    }).then((orders) => {
                        ItemModel
                        .find({restaurant: req.params.id})
                        .then((items)=>{
                            let dashboardObj ={
                                type:"owner",
                                todayOrders: specificOrders,
                                orders: orders,
                                items: items,
                            }
                            return apiResponse.successResponseWithData(res, "Operation success", dashboardObj);
                        })
                    })
                })    
            }else{
                Restaurant.find({}).then((restaurant)=>{                
                    UserModel.find({user_type:"teleadmin",active:true}).then((teleUser)=>{
                        UserModel.find({user_type:"owner",active:true}).then((ownerUser)=>{
                            UserModel.find({active:true}).then((inActiveUser)=>{
                                let dashboardObj ={
                                    type:"teleadmin",
                                    restaurant: restaurant,
                                    admin: teleUser,
                                    owner: ownerUser,
                                    inactive: inActiveUser
                                }
                                return apiResponse.successResponseWithData(res, "Operation success", dashboardObj);
                            });
                        });
                    });
                });
            }
		} catch (err) {
			//throw error in json response with status 500. 
			return apiResponse.ErrorResponse(res, err);
		}
	}
];

// exports.ownerDashboard = [
// 	auth,
// 	accessValidation,
// 	function (req, res) {
// 		if(!mongoose.Types.ObjectId.isValid(req.params.id)){
// 			return apiResponse.successResponseWithData(res, "Operation success", {});
// 		}
// 		try {
//             let start = new Date(new Date()).setHours(00, 00, 00)
//             let end = new Date(new Date()).setHours(23, 59, 59)
// 			OrderModel.find({ 
//                 restaurant: req.params.id, 
//                 payment_status: true,
//                 createdAt:{
//                   "$gte": start,
//                   "$lt": end
//                 }
//             }).then((specificOrders) => {
//                 // specificOrders.reduce(
//                 //     (preOrder, nextOrder) => preOrder.total_price + nextOrder.total_price, 0
//                 // )
//                 OrderModel.find({ 
//                     restaurant: req.params.id, 
//                     payment_status: true,
//                 }).then((orders) => {
//                     ItemModel
//                     .find({restaurant: req.params.id})
//                     .then((items)=>{
//                         let dashboardObj ={
//                             restaurant: restaurant.length,
//                             admin: teleUser.length,
//                             owner: ownerUser.length,
//                             inactive: inActiveUser.length
//                         }
//                         return apiResponse.successResponseWithData(res, "Operation success", dashboardObj);
//                     })
//                 })
//             })
// 		} catch (err) {
// 			//throw error in json response with status 500. 
// 			return apiResponse.ErrorResponse(res, err);
// 		}
// 	}
// ];
