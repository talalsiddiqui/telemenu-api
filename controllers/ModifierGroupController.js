const ModifierModel = require("../models/ModifierModel"); 
const ModifierGroupModel = require("../models/ModifierGroupModel");
const Restaurant = require("../models/RestaurantModel");
const { body,validationResult } = require("express-validator");
const { sanitizeBody } = require("express-validator");
const apiResponse = require("../helpers/apiResponse");
const auth = require("../middlewares/jwt");
const accessValidation = require("../middlewares/userValidation");
var mongoose = require("mongoose");
mongoose.set("useFindAndModify", false);

/**
 * Category List. for specific restaurant
 * 
 * @returns {Object}
 */

exports.modifierGroupList = [
	auth,
	accessValidation,
	function (req, res) {
		try {
			console.log(req.user._id)
			if (req.user.user_type ==="owner"){
				Restaurant
				.find({owner: req.user._id},'_id owner')
				.then((restaurantList)=>{
					if(restaurantList === null){
						return apiResponse.notFoundResponse(res,"Restaurant not exists with this id");
					}else{
						restaurantIDs = restaurantList.map((singleRestaurant)=>{
							return singleRestaurant._id
						})
						ModifierGroupModel
						.find({restaurant: {$in:restaurantIDs}}, "_id name type restaurant modifier active")
						.populate([{
							path: 'restaurant',
							select: '_id name'
                        }])
                        .populate('modifier')
						.then((modifiergroups)=>{
							if(modifiergroups.length > 0){
								return apiResponse.successResponseWithData(res, "List fetched success", modifiergroups);
							}else{
								return apiResponse.successResponseWithData(res, "No data found", []);
							}
						});
					}
				})
			}else{
				console.log("teleadmin working")
				ModifierGroupModel
				.find({},"_id name type restaurant modifier active")
                .populate('restaurant','name')
                .populate('modifier','name')
				.then((modifiergroups)=>{
					if(modifiergroups.length > 0){
						console.log("workin fine teleadmin")
						return apiResponse.successResponseWithData(res, "List fetched success", modifiergroups);
					}else{
						console.log("workin fine teleadminno data found")
						return apiResponse.successResponseWithData(res, "No data found", []);
					}
				});
			}
		} catch (err) { 
			console.log("telemenu error")
			return apiResponse.ErrorResponse(res, err);
		}
	}
];

/**
 * Modifier Group store.
 * 
 * @param {string}      title 
 * @param {string}      description
 * @param {string}      isbn
 * 
 * @returns {Object}
 */
exports.modifierGroupStore = [
	auth,
	accessValidation,
	body("name", "Name must not be empty.").isLength({ min: 1 }).trim(),
	body("type", "Type must not be empty.").isLength({ min: 1 }).trim(),
	body("restaurant", "Restaurant must not be empty.").isLength({ min: 1 }).trim(),
	body('modifier').toArray(),
	sanitizeBody("name").escape(),
	sanitizeBody("type").escape(),
	sanitizeBody("restaurant").escape(),
	(req, res) => {
		try {
			const errors = validationResult(req);
            console.log("custom array",req.body)
            console.log("modifier",req.body.modifier.toString())
            var modifiergroup = new ModifierGroupModel(
				{   
                    name: req.body.name,
					type: req.body.type,
                    restaurant: req.body.restaurant,
                    modifier: req.body.modifier
                });
			if (!errors.isEmpty()) {
                console.log("error after instance",errors)
				return apiResponse.validationErrorWithData(res, "Validation Error.", errors.array());
			}
			else {             
                //Save Modifier.
				modifiergroup.save(function (err,modifierGroupDoc) {
					if (err)  return apiResponse.ErrorResponse(res, err); 
                    console.log("controller end",modifierGroupDoc)
                    if (req.user.user_type ==="owner"){
						Restaurant
						.find({owner: req.user._id},'_id owner')
						.then((restaurantList)=>{
							if(restaurantList === null){
								return apiResponse.notFoundResponse(res,"Restaurant not exists with this id");
							}else{
								restaurantIDs = restaurantList.map((singleRestaurant)=>{
									return singleRestaurant._id
								})
								ModifierGroupModel
								.find({restaurant: {$in:restaurantIDs}}, "_id name type restaurant modifier active")
								.populate([{
									path: 'restaurant',
									select: '_id name'
                                }])
                                .populate('modifier')
								.then((modidiergroups)=>{
									if(modidiergroups.length > 0){
										return apiResponse.successResponseWithData(res, "Modifier Group add success", modidiergroups);
									}else{
										return apiResponse.successResponseWithData(res, "Modifier Group add success", []);
									}
								});
							}
						})
					}else{
						ModifierGroupModel
						.find({},"_id name type restaurant modifier active")
                        .populate('restaurant','name')
                        .populate('modifier')
						.then((modidiergroups)=>{
							if(modidiergroups.length > 0){
								return apiResponse.successResponseWithData(res, "Modifier Group add success", modidiergroups);
							}else{
								return apiResponse.successResponseWithData(res, "Modifier Group add success", []);
							}
						});
					}
				});
			}
		} catch (err) {
			//throw error in json response with status 500. 
			return apiResponse.ErrorResponse(res, err);
		}
	}
];

/**
 * Modifier Group update.
 * 
 * @param {string}      title 
 * @param {string}      description
 * @param {string}      isbn
 * 
 * @returns {Object}
 */
exports.modifierGroupUpdate = [
	auth,
	accessValidation,
	body("name", "Name must not be empty.").isLength({ min: 1 }).trim(),
	body("type", "Type must not be empty.").isLength({ min: 1 }).trim(),
	body("restaurant", "Restaurant must not be empty.").isLength({ min: 1 }).trim(),
	body("modifier", "Modifier must not be Select.").toArray(),
	sanitizeBody("name").escape(),
	sanitizeBody("type").escape(),
	sanitizeBody("restaurant").escape(),
	(req, res) => {
		try {
			const errors = validationResult(req);
			console.log(req.body)
			var modifierGroup = new ModifierGroupModel(
                {   
                    name: req.body.name,
					type: req.body.type,
					restaurant: req.body.restaurant,
					modifier: req.body.modifier,
					_id:req.params.id
				});
			if (!errors.isEmpty()) {
				return apiResponse.validationErrorWithData(res, "Validation Error.", errors.array());
			}
			else {
				if(!mongoose.Types.ObjectId.isValid(req.params.id)){
					return apiResponse.validationErrorWithData(res, "Invalid Error.", "Invalid modifier Group ID");
				}else{
					ModifierGroupModel.findById(req.params.id, function (err, foundModifierGroup) {
						if(foundModifierGroup === null){
							return apiResponse.notFoundResponse(res,"Modifier Group not exists with this id");
						}else{
							//update restaurant.
							ModifierGroupModel.findByIdAndUpdate(req.params.id, modifierGroup, {},function (err) {
								if (err) { 
									return apiResponse.ErrorResponse(res, err); 
								}else{
									if (req.user.user_type ==="owner"){
										Restaurant
										.find({owner: req.user._id},'_id owner')
										.then((restaurantList)=>{
											if(restaurantList === null){
												return apiResponse.notFoundResponse(res,"Restaurant not exists with this id");
											}else{
												restaurantIDs = restaurantList.map((singleRestaurant)=>{
													return singleRestaurant._id
												})
												ModifierGroupModel
												.find({restaurant: {$in:restaurantIDs}}, "_id name type restaurant modifier active")
												.populate([{
													path: 'restaurant',
													select: '_id name'
												}])
												.populate('modifier')
												.then((modifiergroups)=>{
													if(modifiergroups.length > 0){
														return apiResponse.successResponseWithData(res, "Modifier group update success", modifiergroups);
													}else{
														return apiResponse.successResponseWithData(res, "Modifier group update success", []);
													}
												});
											}
										})
									}else{
										console.log("teleadmin working")
										ModifierGroupModel
										.find({},"_id name type restaurant modifier active")
										.populate('restaurant','name')
										.populate('modifier','name')
										.then((modifiergroups)=>{
											if(modifiergroups.length > 0){
												console.log("workin fine teleadmin")
												return apiResponse.successResponseWithData(res, "Modifier group update success", modifiergroups);
											}else{
												console.log("workin fine teleadminno data found")
												return apiResponse.successResponseWithData(res, "Modifier group update success", []);
											}
										});
									}
								}
							});
						}
					});
				}
			}
		} catch (err) {
			//throw error in json response with status 500. 
			return apiResponse.ErrorResponse(res, err);
		}
	}
];

/**
 * Modifier Group Delete.
 * 
 * @param {string}      id
 * 
 * @returns {Object}
 */
exports.modifierGroupDelete = [
	auth,
	accessValidation,
	function (req, res) {
		console.log(req.params)
		if(!mongoose.Types.ObjectId.isValid(req.params.id)){
			return apiResponse.validationErrorWithData(res, "Invalid Error.", "Invalid ID");
		}
		try {
			ModifierGroupModel.findById(req.params.id, function (err, foundModifierGroup) {
				if(foundModifierGroup === null){
					return apiResponse.notFoundResponse(res,"Modifier Group not exists with this id");
				}else{
					//delete Restaurant.
					ModifierGroupModel.findByIdAndRemove(req.params.id,function (err) {
						if (err) { 
							return apiResponse.ErrorResponse(res, err); 
						}else{
							if (req.user.user_type ==="owner"){
								Restaurant
								.find({owner: req.user._id},'_id owner')
								.then((restaurantList)=>{
									if(restaurantList === null){
										return apiResponse.notFoundResponse(res,"Restaurant not exists with this id");
									}else{
										restaurantIDs = restaurantList.map((singleRestaurant)=>{
											return singleRestaurant._id
										})
										ModifierGroupModel
										.find({restaurant: {$in:restaurantIDs}}, "_id name type restaurant modifier active")
										.populate([{
											path: 'restaurant',
											select: '_id name'
										}])
										.populate('modifier')
										.then((modifiergroups)=>{
											if(modifiergroups.length > 0){
												return apiResponse.successResponseWithData(res, "Modifier group delete success", modifiergroups);
											}else{
												return apiResponse.successResponseWithData(res, "Modifier group delete success", []);
											}
										});
									}
								})
							}else{
								console.log("teleadmin working")
								ModifierGroupModel
								.find({},"_id name type restaurant modifier active")
								.populate('restaurant','name')
								.populate('modifier','name')
								.then((modifiergroups)=>{
									if(modifiergroups.length > 0){
										console.log("workin fine teleadmin")
										return apiResponse.successResponseWithData(res, "Modifier group delete success", modifiergroups);
									}else{
										console.log("workin fine teleadminno data found")
										return apiResponse.successResponseWithData(res, "Modifier group delete success", []);
									}
								});
							}
						}
					});
				}
			});
		} catch (err) {
			//throw error in json response with status 500. 
			return apiResponse.ErrorResponse(res, err);
		}
	}
];

exports.updateModifierGroupActiveness = [
	auth,
	accessValidation,
	body("active")
	  .isLength({ min: 1 })
	  .withMessage("Active Status must be specified."),
	// Sanitize fields.
	sanitizeBody("active").escape(),
	// Process request after validation and sanitization.
	(req, res) => {
	  if(!mongoose.Types.ObjectId.isValid(req.params.id)){
		console.log("id error")
		return apiResponse.validationErrorWithData(
		  res,
		  "Invalid Error.",
		  "Invalid ID"
		);
	  }
	  try {
		console.log(req.body)
		console.log(req.params)
		// Extract the validation errors from a request.
		const errors = validationResult(req);
		if (!errors.isEmpty()) {
		  console.log(errors)
		  // Display sanitized values/errors messages.
		  return apiResponse.validationErrorWithData(
			res,
			"Validation Error.",
			errors.array()
		  );
		} else {
		  // Create User object with escaped and trimmed data
		  if (!errors.isEmpty()) {
			console.log("error")
			return apiResponse.validationErrorWithData(res, "Validation Error.", errors.array());
		  }else{
			console.log("working 2")
			ModifierGroupModel.findById(req.params.id, function (err, foundModifierGroup) {
			  if(foundModifierGroup === null){
				console.log("user find error")
				return apiResponse.notFoundResponse(res,"User not exists with this id");
			  }else{
				console.log("working 3")
				ModifierGroupModel.findByIdAndUpdate(req.params.id, {active:req.body.active}, {},function (err) {
				  if (err) { 
					return apiResponse.ErrorResponse(res, err); 
				  }else{
					if (req.user.user_type ==="owner"){
						Restaurant
						.find({owner: req.user._id},'_id owner')
						.then((restaurantList)=>{
							if(restaurantList === null){
								return apiResponse.notFoundResponse(res,"Restaurant not exists with this id");
							}else{
								restaurantIDs = restaurantList.map((singleRestaurant)=>{
									return singleRestaurant._id
								})
								ModifierGroupModel
								.find({restaurant: {$in:restaurantIDs}}, "_id name type restaurant modifier active")
								.populate([{
									path: 'restaurant',
									select: '_id name'
								}])
								.populate('modifier')
								.then((modifiergroups)=>{
									if(modifiergroups.length > 0){
										return apiResponse.successResponseWithData(res, "Modifier group status update success", modifiergroups);
									}else{
										return apiResponse.successResponseWithData(res, "Modifier group status update success", []);
									}
								});
							}
						})
					}else{
						console.log("teleadmin working")
						ModifierGroupModel
						.find({},"_id name type restaurant modifier active")
						.populate('restaurant','name')
						.populate('modifier','name')
						.then((modifiergroups)=>{
							if(modifiergroups.length > 0){
								console.log("workin fine teleadmin")
								return apiResponse.successResponseWithData(res, "Modifier group status update success", modifiergroups);
							}else{
								console.log("workin fine teleadminno data found")
								return apiResponse.successResponseWithData(res, "Modifier group status update success", []);
							}
						});
					}
				  }
				});
			  }
			})
		  }
		}
	  } catch (err) {
		//throw error in json response with status 500.
		return apiResponse.ErrorResponse(res, err);
	  }
	},
  ];