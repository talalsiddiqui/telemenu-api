const WaiterCallModel = require("../models/WaiterCallModel"); 
const Restaurant = require("../models/RestaurantModel");
const { body,validationResult } = require("express-validator");
const { sanitizeBody } = require("express-validator");
const apiResponse = require("../helpers/apiResponse");
const auth = require("../middlewares/jwt");
const accessValidation = require("../middlewares/userValidation");
var mongoose = require("mongoose");
mongoose.set("useFindAndModify", false);

/**
 * Category List. for specific restaurant
 * 
 * @returns {Object}
 */

exports.waiterCallList = [
	auth,
	accessValidation,
	function (req, res) {
		if (!mongoose.Types.ObjectId.isValid(req.params.id)) {
			return apiResponse.validationErrorWithData(
			  res,
			  "Invalid Id",
			  ""
			);
		}
		try {
			console.log(req.user._id)
			Restaurant.findById(req.params.id, function (err, foundRestaurant) {
				if(!foundRestaurant) return apiResponse.notFoundResponse(res,"Restaurant not exists with this id");
			})
			WaiterCallModel.find({ restaurant: req.params.id, fullfilled:false })
            .then((WaiterCallDetail) => {
              if (WaiterCallDetail.length > 0) {
                return apiResponse.successResponseWithData(
                  res,
                  "List fetched success",
                  WaiterCallDetail
                );
              } else {
                return apiResponse.successResponseWithData(
                  res,
                  "No data found",
                  []
                );
              }
            });
		} catch (err) { 
			return apiResponse.ErrorResponse(res, err);
		}
	}
];

/**
 * Category store.
 * 
 * @param {string}      title 
 * @param {string}      description
 * @param {string}      isbn
 * 
 * @returns {Object}
 */
exports.WaiterCallStore = [
	// auth,
	// accessValidation,
    body("table_no", "Table Number must not be empty.").isLength({ min: 1 }).trim(),
	body("restaurant", "Restaurant must not be empty.")
	.isLength({ min: 1 }).trim()
	.custom((value) => {
		return Restaurant.findById(value).then((restaurant) => {
			if(!restaurant){
			  console.log("email exist error")
			  return Promise.reject("Restaurant does not exist");
			}
		});
	}),
	sanitizeBody("table_no").escape(),
	sanitizeBody("restaurant").escape(),
	(req, res) => {
		try {
            console.log(req.body)
            const errors = validationResult(req);
			var waiterCall = new WaiterCallModel({   
                table_no: req.body.table_no,
                restaurant: req.body.restaurant,
                description: req.body.description,
                plate: req.body.plate,
                spoon:req.body.spoon,
                fork:req.body.fork,
                knife:req.body.knife,
				tissue:req.body.tissue,
				payment:req.body.payment,
				order:req.body.order
            });
			if (!errors.isEmpty()) {
				return apiResponse.validationErrorWithData(res, "Validation Error.", errors.array());
			}
			else {             
                //Save Waiter Call Message.
				waiterCall.save(function (err,WaiterCall) {
					if (err)  return apiResponse.ErrorResponse(res, err); 
                    return apiResponse.successResponseWithData(res, "Waiter Call Message add success", []);
                    // if(WaiterCall.length > 0){
                    //     return apiResponse.successResponseWithData(res, "Waiter Call Message add success", WaiterCall);
                    // }else{
                    //     return apiResponse.successResponseWithData(res, "Waiter Call Message add success", []);
                    // }
				});
			}
		} catch (err) {
			//throw error in json response with status 500. 
			return apiResponse.ErrorResponse(res, err);
		}
	}
];

/**
 * Category update.
 * 
 * @param {string}      title 
 * @param {string}      description
 * @param {string}      isbn
 * 
 * @returns {Object}
 */
exports.waiterCallFulfill = [
	auth,
	accessValidation,
	sanitizeBody("*").escape(),
	(req, res) => {
		try {
			const errors = validationResult(req);
			console.log(req.body)
			if (!errors.isEmpty()) {
				return apiResponse.validationErrorWithData(res, "Validation Error.", errors.array());
			}
			else {
				if(!mongoose.Types.ObjectId.isValid(req.params.id)){
					return apiResponse.validationErrorWithData(res, "Invalid Error.", "Invalid Waiter Call ID");
				}else{
					WaiterCallModel.findById(req.params.id, function (err, foundWaiterCall) {
						if(foundWaiterCall === null){
							return apiResponse.notFoundResponse(res,"Waiter Call not exists with this id");
						}else{
							//update restaurant.
							WaiterCallModel.findByIdAndUpdate(req.params.id, {fullfilled:true}, {},function (err) {
								if (err) { 
									return apiResponse.ErrorResponse(res, err); 
								}else{
									WaiterCallModel.find({ restaurant: req.body.Rid, fullfilled:false })
									.then((WaiterCallDetail) => {
										if (WaiterCallDetail.length > 0) {
											return apiResponse.successResponseWithData(
												res,
												"List fetched success",
												WaiterCallDetail
											);
										} else {
											return apiResponse.successResponseWithData(
												res,
												"No data found",
												[]
											);
										}
									});
								}
							});
						}
					});
				}
			}
		} catch (err) {
			//throw error in json response with status 500. 
			return apiResponse.ErrorResponse(res, err);
		}
	}
];
