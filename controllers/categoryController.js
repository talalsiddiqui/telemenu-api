const Restaurant = require("../models/RestaurantModel");
const { body,validationResult } = require("express-validator");
const { sanitizeBody } = require("express-validator");
const apiResponse = require("../helpers/apiResponse");
const auth = require("../middlewares/jwt");
const accessValidation = require("../middlewares/userValidation");
var mongoose = require("mongoose");
const CategoryModel = require("../models/CategoryModel");
const ItemModel = require("../models/ItemModel");
const ModifierGroupModel = require("../models/ModifierGroupModel");

mongoose.set("useFindAndModify", false);

/**
 * Category List. for specific restaurant
 * 
 * @returns {Object}
 */
exports.specificRestaurantcategoryList = [
	auth,
	accessValidation,
	function (req, res) {
		try {
            console.log(req.params.id)
			CategoryModel.find({restaurant: req.params.id},"_id name").then((categories)=>{
				console.log(categories)
				if(categories.length > 0){
					ModifierGroupModel.find({restaurant: req.params.id},"_id name").then((modifierGroups)=>{
						if(modifierGroups.length > 0){	
							return apiResponse.successResponseWithData(res, "List fetched", {categories,modifierGroups});
						}else{
							return apiResponse.successResponseWithData(res, "List fetched", {categories});
						}
					});
				}else{
					return apiResponse.successResponseWithData(res, "No data found", []);
				}
			});
		} catch (err) { 
			return apiResponse.ErrorResponse(res, err);
		}
	}
];

exports.categoryList = [
	auth,
	accessValidation,
	function (req, res) {
		try {
			if (req.user.user_type ==="owner"){
				Restaurant
				.find({owner: req.user._id},'_id owner')
				.then((restaurantList)=>{
					if(restaurantList === null){
						return apiResponse.notFoundResponse(res,"Restaurant not exists with this id");
					}else{
						restaurantIDs = restaurantList.map((singleRestaurant)=>{
							return singleRestaurant._id
						})
						CategoryModel
						.find({restaurant: {$in:restaurantIDs}}, "_id name description restaurant")
						.populate([{
							path: 'restaurant',
							select: '_id name'
						}])
						.then((categories)=>{
							if(categories.length > 0){
								return apiResponse.successResponseWithData(res, "List fetched success", categories);
							}else{
								return apiResponse.successResponseWithData(res, "No data found", []);
							}
						});
					}
				})
			}else{
				CategoryModel
				.find({},"_id name description restaurant")
				.populate('restaurant','name')
				.then((categories)=>{
					if(categories.length > 0){
						return apiResponse.successResponseWithData(res, "List fetched success", categories);
					}else{
						return apiResponse.successResponseWithData(res, "No data found", []);
					}
				});
			}        
		} catch (err) { 
			return apiResponse.ErrorResponse(res, err);
		}
	}
];


/**
 * Category Detail.
 * 
 * @param {string}      id
 * 
 * @returns {Object}
 */
exports.categoryDetail = [
	auth,
	accessValidation,
	function (req, res) {
		if(!mongoose.Types.ObjectId.isValid(req.params.id)){
			return apiResponse.successResponseWithData(res, "Operation success", {});
		}
		try {
			Restaurant.findOne({_id: req.params.id,user: req.user._id},"_id title description isbn createdAt").then((restaurant)=>{                
				if(restaurant !== null){
					let restaurantData = new RestaurantData(restaurant);
					return apiResponse.successResponseWithData(res, "Operation success", restaurantData);
				}else{
					return apiResponse.successResponseWithData(res, "Operation success", {});
				}
			});
		} catch (err) {
			//throw error in json response with status 500. 
			return apiResponse.ErrorResponse(res, err);
		}
	}
];

/**
 * Category store.
 * 
 * @param {string}      title 
 * @param {string}      description
 * @param {string}      isbn
 * 
 * @returns {Object}
 */
exports.categoryStore = [
	auth,
	accessValidation,
	body("name", "Name must not be empty.").isLength({ min: 1 }).trim(),
	body("restaurant", "Restaurant must not be Select.").isLength({ min: 1 }).trim(),
	sanitizeBody("*").escape(),
	(req, res) => {
		try {
            const errors = validationResult(req);
			var category = new CategoryModel(
				{   
                    name: req.body.name,
					description: req.body.description,
					restaurant: req.body.restaurant,
                });
			if (!errors.isEmpty()) {
				return apiResponse.validationErrorWithData(res, "Validation Error.", errors.array());
			}
			else {             
                //Save Restaurant.
				category.save(function (err,categoryDoc) {
					if (err)  return apiResponse.ErrorResponse(res, err); 
					if (req.user.user_type ==="owner"){
						Restaurant
						.find({owner: req.user._id},'_id owner')
						.then((restaurantList)=>{
							if(restaurantList === null){
								return apiResponse.notFoundResponse(res,"Restaurant not exists with this id");
							}else{
								restaurantIDs = restaurantList.map((singleRestaurant)=>{
									return singleRestaurant._id
								})
								CategoryModel
								.find({restaurant: {$in:restaurantIDs}}, "_id name description restaurant")
								.populate([{
									path: 'restaurant',
									select: '_id name'
								}])
								.then((afterSavedcategoryList)=>{
									if(afterSavedcategoryList.length > 0){
										return apiResponse.successResponseWithData(res, "Category add success", afterSavedcategoryList);
									}else{
										return apiResponse.successResponseWithData(res, "Category add success", []);
									}
								});
							}
						})
					}else{
						CategoryModel
						.find({},"_id name description restaurant")
						.populate('restaurant','name')
						.then((afterSavedcategoryList)=>{
							if(afterSavedcategoryList.length > 0){
								return apiResponse.successResponseWithData(res, "Category add success", afterSavedcategoryList);
							}else{
								return apiResponse.successResponseWithData(res, "Category add success", []);
							}
						});
					}
				});
			}
		} catch (err) {
			//throw error in json response with status 500. 
			return apiResponse.ErrorResponse(res, err);
		}
	}
];

/**
 * Category update.
 * 
 * @param {string}      title 
 * @param {string}      description
 * @param {string}      isbn
 * 
 * @returns {Object}
 */
exports.categoryUpdate = [
	auth,
	accessValidation,
	body("name", "Name must not be empty.").isLength({ min: 1 }).trim(),
	body("restaurant", "Restaurant must not be Select.").isLength({ min: 1 }).trim(),
	sanitizeBody("*").escape(),
	(req, res) => {
		try {
			const errors = validationResult(req);
			console.log(req.body)
			var category = new CategoryModel(
                {   
                    name: req.body.name,
					description: req.body.description,
					restaurant: req.body.restaurant,
					_id:req.params.id
				});
			if (!errors.isEmpty()) {
				return apiResponse.validationErrorWithData(res, "Validation Error.", errors.array());
			}
			else {
				if(!mongoose.Types.ObjectId.isValid(req.params.id)){
					return apiResponse.validationErrorWithData(res, "Invalid Error.", "Invalid ID");
				}else{
					CategoryModel.findById(req.params.id, function (err, foundCategory) {
						if(foundCategory === null){
							return apiResponse.notFoundResponse(res,"Category not exists with this id");
						}else{
							//update restaurant.
							CategoryModel.findByIdAndUpdate(req.params.id, category, {},function (err) {
								if (err) { 
									return apiResponse.ErrorResponse(res, err); 
								}else{
									if(req.user.user_type ==="owner"){
										Restaurant
										.find({owner: req.user._id},'_id owner')
										.then((restaurantList)=>{
											if(restaurantList === null){
												return apiResponse.notFoundResponse(res,"Restaurant not exists with this id");
											}else{
												restaurantIDs = restaurantList.map((singleRestaurant)=>{
													return singleRestaurant._id
												})
												CategoryModel
												.find({restaurant: {$in:restaurantIDs}}, "_id name description restaurant")
												.populate([{
													path: 'restaurant',
													select: '_id name'
												}])
												.then((afterEditcategoryList)=>{
													if(afterEditcategoryList.length > 0){
														return apiResponse.successResponseWithData(res, "Category update success", afterEditcategoryList);
													}else{
														return apiResponse.successResponseWithData(res, "Category update success", []);
													}
												});
											}
										})
									}else{
										CategoryModel
										.find({},"_id name description restaurant")
										.populate('restaurant','name')
										.then((afterEditcategoryList)=>{
											if(afterEditcategoryList.length > 0){
												return apiResponse.successResponseWithData(res, "Category update success", afterEditcategoryList);
											}else{
												return apiResponse.successResponseWithData(res, "Category update success", []);
											}
										});
									}
								}
							});
						}
					});
				}
			}
		} catch (err) {
			//throw error in json response with status 500. 
			return apiResponse.ErrorResponse(res, err);
		}
	}
];

/**
 * Category Delete.
 * 
 * @param {string}      id
 * 
 * @returns {Object}
 */
exports.categoryDelete = [
	auth,
	accessValidation,
	function (req, res) {
		console.log(req.params)
		if(!mongoose.Types.ObjectId.isValid(req.params.id)){
			return apiResponse.validationErrorWithData(res, "Invalid Error.", "Invalid ID");
		}
		try {
			CategoryModel.findById(req.params.id, function (err, foundCategory) {
				if(foundCategory === null){
					return apiResponse.notFoundResponse(res,"Category not exists with this id");
				}else{
					//delete Restaurant.
					ItemModel.find({category: req.params.id}, function (err, itemCheck) {
						if(itemCheck.length>0){
							return apiResponse.cascadeErrorWithData(res, "Invalid Attempt. Delete restaurant associate Items first", "");
						}else{
							CategoryModel.findByIdAndRemove(req.params.id,function (err) {
								if (err) { 
									return apiResponse.ErrorResponse(res, err); 
								}else{
									if (req.user.user_type ==="owner"){
										Restaurant
										.find({owner: req.user._id},'_id owner')
										.then((restaurantList)=>{
											if(restaurantList === null){
												return apiResponse.notFoundResponse(res,"Restaurant not exists with this id");
											}else{
												restaurantIDs = restaurantList.map((singleRestaurant)=>{
													return singleRestaurant._id
												})
												CategoryModel
												.find({restaurant: {$in:restaurantIDs}}, "_id name description restaurant")
												.populate([{
													path: 'restaurant',
													select: '_id name'
												}])
												.then((afterdeletecategoryList)=>{
													if(afterdeletecategoryList.length > 0){
														return apiResponse.successResponseWithData(res, "Category delete success.", afterdeletecategoryList);
													}else{
														return apiResponse.successResponseWithData(res, "Category delete success.", []);
													}
												});
											}
										})
									}else{
										CategoryModel
										.find({},"_id name description restaurant")
										.populate('restaurant','name')
										.then((afterdeletecategoryList)=>{
											if(afterdeletecategoryList.length > 0){
												return apiResponse.successResponseWithData(res, "Category delete success.", afterdeletecategoryList);
											}else{
												return apiResponse.successResponseWithData(res, "Category delete success.", []);
											}
										});
									}
								}
							});
						}
					})
				}
			});
		} catch (err) {
			//throw error in json response with status 500. 
			return apiResponse.ErrorResponse(res, err);
		}
	}
];