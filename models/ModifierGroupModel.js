var mongoose = require("mongoose");

var Schema = mongoose.Schema;

var ModifierGroupSchema = new Schema({
	name: {type: String, required: true},
	type: {type: String, required: true},
    restaurant:{ type: Schema.Types.ObjectId, ref: "Restaurant", required: true },
    modifier: [{ type: Schema.Types.ObjectId, ref: "Modifier"}],
    active: {type: Boolean, required: true, default: 1},
}, {timestamps: true});

module.exports = mongoose.model("ModifierGroup", ModifierGroupSchema);