var mongoose = require("mongoose");

var Schema = mongoose.Schema;

var WaiterCallSchema = new Schema({
    table_no: {type: String, required: true},
    restaurant: {type: Schema.Types.ObjectId, ref: "Restaurant", required: true },
    description: {type: String},
    fullfilled: {type: Boolean,default:0},
    plate:{type:Number,default:0},
    spoon:{type:Number,default:0},
    fork:{type:Number,default:0},
    knife:{type:Number,default:0},
    tissue:{type:Number,default:0},
    payment:{type: Boolean,default:0},
    order:[ {type: Schema.Types.ObjectId, ref: "Order"} ],
}, {timestamps: true});

module.exports = mongoose.model("WaiterCall", WaiterCallSchema);