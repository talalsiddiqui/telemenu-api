var mongoose = require("mongoose");

var Schema = mongoose.Schema;

var OrderSchema = new Schema({
    restaurant:{ type: Schema.Types.ObjectId, ref: "Restaurant", required: true },
    table_no:{ type: String, required: true },
    mobile:{ type: Number },
    order_date:{ type: Date, required: true },
    total_qty:{ type: Number, required: true },
    order_id:{ type: String, required: true },
    // coupon:{type :Schema.Types.ObjectId, ref: "Coupon"},
    // coupon_discount:{type: Number, required: true"},
    // total_discount:{type: Number, required: true},
    // total_tax:{ type: Number, required: true },
    total_price:{ type: Number, required: true },
    // grand_total:{ type: Number, required: true },
    // delivery_cost:{ type: Number, required: true},
    payment_status:{ type:Boolean, default:false },
    fulfilled:{ type:Boolean, default:false },
    is_pending:{ type:Boolean, default:true },
    in_progress:{ type:Boolean, default:false },
    is_ready:{ type:Boolean, default:false },
    is_cancel:{ type:Boolean, default:false },
    is_deleted:{ type:Boolean, default:false },
    is_active:{ type:Boolean, default:true}
}, {timestamps: true});

module.exports = mongoose.model("Order", OrderSchema);