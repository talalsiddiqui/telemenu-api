var mongoose = require("mongoose");
var Schema = mongoose.Schema;
var ItemSchema = new Schema({
    name: { type: String, required: true },
    urdu_name:{ type: String, default:"" },
    description: { type: String },
    urdu_description: { type: String },
    price: { type: Number, required: true },
    restaurant:{ type: Schema.Types.ObjectId, ref: "Restaurant", required: true },
    category: { type: Schema.Types.ObjectId, ref: "Category", required: true },
    modifierGroup:  [{ type: Schema.Types.ObjectId, ref: "ModifierGroup" }],
    item_image: { 
        path:{type: String},
        name:{type: String},
    },
    active: { type: Boolean, required: true, default: 1},
    trans_id:{ type: String},
    trans_des_id:{ type: String},
    no_gluten: { type: Boolean, required: true, default: 0},
    no_nuts: { type: Boolean, required: true, default: 0},
    no_shellfish: { type: Boolean, required: true, default: 0},
    vegan: { type: Boolean, required: true, default: 0},
    vegetarian: { type: Boolean, required: true, default: 0},
    keto: { type: Boolean, required: true, default: 0},
    custom_schedule:{ type: Boolean, required: true, default: 0},
    start_time:{ type: Date },
    end_time:{ type: Date },
    sell_count:{type: Number,default: 0},
}, {timestamps: true});

module.exports = mongoose.model("Item", ItemSchema);