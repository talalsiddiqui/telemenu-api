var mongoose = require("mongoose");

var Schema = mongoose.Schema;

var OrderDetailSchema = new Schema({
    order:{ type: Schema.Types.ObjectId, ref: "Order", required: true},
    items:{ type: Schema.Types.ObjectId, ref: "Item"},
    modifiers:[{ type :Schema.Types.ObjectId, ref: "Modifier"}],
    restaurant: { type: Schema.Types.ObjectId, ref: "Restaurant", required: true },
    modifierData:[{
        _id:{type: String},
        name:{type:String},
        price:{type:Number}
    }],
    is_canceled:{type:Boolean,default:0},
    qty:{ type: Number, required: true },
    unit_price:{ type: Number, required: true },
    // discount:{ type: Number, default:0 },
    // tax_rate:{ type: Number, default:0 },
    // tax:{ type: Number, default:0 },
    // total:{ type: Number, required: true },
}, {timestamps: true});

module.exports = mongoose.model("OrderDetail", OrderDetailSchema);