var mongoose = require("mongoose");
var Schema = mongoose.Schema;

var MenuSchema = new Schema({
	name: {type: String, required: true},
    description: {type: String, default:""},
    order_message: {type: String, required: true},
    busy_message: {type: String, required: true},
    is_busy: {type: Boolean, default: 0},
    restaurant: { type: Schema.Types.ObjectId, ref: "Restaurant", required: true },
    sections: [{ type: Schema.Types.ObjectId, ref: "Section", required: true }]
}, {timestamps: true});

module.exports = mongoose.model("Menu", MenuSchema);