var mongoose = require("mongoose");
var Schema = mongoose.Schema;

var PaymentSchema = new Schema({
	credit_card: {type: String},
	debit_card: {type: String},
    cash_on_delivery: {type: String},   
}, {timestamps: true});

module.exports = mongoose.model("Payment", PaymentSchema);