var mongoose = require("mongoose");

var Schema = mongoose.Schema;

var SectionSchema = new Schema({
    name: {type: String, required: true},
    urdu_name:{ type: String, default:"" },
	description: {type: String, default:""},
    restaurant:{ type: Schema.Types.ObjectId, ref: "Restaurant", required: true },
    is_parent:{type: Boolean, required: true},
    items:[{ type: Schema.Types.ObjectId, ref: "Item"}],
    parent:{ type: Schema.Types.ObjectId, ref: "Section"},
    trans_id:{type: String},
    custom_schedule:{ type: Boolean, required: true, default: 0},
    start_time:{ type: Date },
    end_time:{ type: Date }
}, {timestamps: true});

module.exports = mongoose.model("Section", SectionSchema);