var mongoose = require("mongoose");
var Schema = mongoose.Schema;

var BankAPIResponseSchema = new Schema({
    order:[{ type: Schema.Types.ObjectId, ref: "Order", required: true }],
    authToken: {type: String, required: true},
    hashKey: {type: String, required: true},
    transactionReferenceNumber:{type: Number, required: true},
    transactionTypeId: {type: Number, required: true},
    isOtp:{ type: Boolean, required: true },
    customerMobile:{type: String, required: true}
}, {timestamps: true});

module.exports = mongoose.model("BankAPIResponse", BankAPIResponseSchema);