var mongoose = require("mongoose");

var Schema = mongoose.Schema;

var ModifierSchema = new Schema({
    name: {type: String, required: true},
    urdu_name:{ type: String, default:"" },
	description: {type: String, default:""},
    price: {type: Number, required: true},
    restaurant:{ type: Schema.Types.ObjectId, ref: "Restaurant", required: true },
    trans_id:{type: String}
}, {timestamps: true});

module.exports = mongoose.model("Modifier", ModifierSchema);