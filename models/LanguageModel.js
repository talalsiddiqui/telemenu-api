var mongoose = require("mongoose");
var Schema = mongoose.Schema;

var LanguageSchema = new Schema({
    restaurant:{ type: Schema.Types.ObjectId, ref: "Restaurant", required: true },
    trans_id: {type: String, required: true},
    en_name: {type: String},
    urdu_name: {type: String},
}, {timestamps: true});

module.exports = mongoose.model("Language", LanguageSchema);