var mongoose = require("mongoose");

var Schema = mongoose.Schema;

var RestaurantSchema = new Schema({
	name: {type: String, required: true},
	address: {type: String, required: true},
    contact_number: {type: String, required: true},
    country: {type: String, required: true},
    restaurant_type: {type: String, required: true},
    currency_type: {type: String, required: true},
    payment_method: {type: String},
    readonly: {type:Boolean,default:0},
    owner: { type: Schema.Types.ObjectId, ref: "User", required: true },
    logo: { 
        path:{type: String},
        name:{type: String},
    },
    banner: { 
        path:{type: String},
        name:{type: String},
    },
}, {timestamps: true});

module.exports = mongoose.model("Restaurant", RestaurantSchema);